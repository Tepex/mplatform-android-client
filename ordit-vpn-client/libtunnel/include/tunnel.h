#ifndef __OVPN_TUNNEL_H
#define __OVPN_TUNNEL_H

int make_stream_socket();


void set_parameter(const char* name, const char* value);

void connect_stream_socket(int stream_socket_fd, const char* peer_address, int peer_port);
void start_tunnel(int tunnel_fd, int stream_socket_fd, const char* resource_directory);
int start_server_tunnel(int tunnel_fd, int stream_socket_fd, const char* resource_directory);
void stop_tunnel(int stream_socket_fd);

#endif
