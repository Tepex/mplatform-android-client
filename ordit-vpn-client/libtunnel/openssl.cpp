#include <arpa/inet.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/rsa.h>
#include <openssl/x509.h>
#include <openssl/evp.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>

#include "openssl.h"
#include <string>

#include <sstream>
#include <iomanip>


//TODO reimplement using SSL_get_ex_data
//openssl x509 -noout -fingerprint -sha512 -in  client-cert.pem
static std::vector<uint8_t> peer_certificate_fingerprint;

/*
struct openssl_error_category : public std::error_category
{
};
*/

void log_traffic(logger& lg, spdlog::level::level_enum loglevel, const char* description, const uint8_t* traffic, size_t count) {

    if (!lg.should_log(loglevel)) {
        return;
    }

    std::stringstream ss;

    ss << description;
    ss << std::hex << std::setfill('0');
    for (size_t i = 0; i < count; i++)
    {
        if (i > 0) {
            ss << ":";
        }
        ss << std::setw(2);
        ss << (int)traffic[i];
    }
    ss << "(" << std::dec << count << ")";
    lg.log(loglevel, ss.str().c_str());
}

int verify_callback(int preverify_ok, X509_STORE_CTX *ctx)
{

    X509   *cert = X509_STORE_CTX_get_current_cert(ctx);
    if (cert == NULL) {
        logger::misc().error("no nonverified certificate found\n");
        return 0;
    }


    uint8_t buf[64];

    const EVP_MD *digest = EVP_sha512();
    unsigned len;

    int rc = X509_digest(cert, digest, (unsigned char*) buf, &len);
    if (rc == 0 || len != 64) {
        logger::misc().error("failed to calculate certificate digest\n");
        return 0;
    }

    if (peer_certificate_fingerprint.size() != 64 || memcmp(buf, peer_certificate_fingerprint.data(), 64) != 0) {
        log_traffic(logger::misc(), spdlog::level::err, "peer certificate fingerpring check failed. peer cert fingerprint:", buf, 64);
        return 0;
    }

    return 1;
}


std::vector<uint8_t> decode_base64(const std::vector<uint8_t>& input) {
    std::vector<uint8_t> ret(input.size()); //output size can not be greater than input size
    int result = EVP_DecodeBlock(ret.data(), input.data(), input.size());
    if (result < 0) {
        logger::misc().error("decode_base64 failed. Input is: %.*s", input.size(), input.data());
        throw_error("decode_base64 failed");
    }
	if (input[input.size() - 1] == '=') {
		result--;
	}
	if (input[input.size() - 2] == '=') {
		result--;
	}

    ret.resize(result);
    return ret;

}

void ssl_stream::init_context(const std::vector<uint8_t>& peer_certificate_fingerprint_base64) 
{
    peer_certificate_fingerprint = decode_base64(peer_certificate_fingerprint_base64);
    if (peer_certificate_fingerprint.size() != 64) {
        lg.error("invalid size of peer fingerprint. Size = %d", peer_certificate_fingerprint.size());
        log_traffic(logger::misc(), spdlog::level::err, "peer cert fingerprint:", peer_certificate_fingerprint.data(), peer_certificate_fingerprint.size());
        throw_error("invalid size of peer fingerprint");
    }

    // Create a new context using DTLS
    dtls.ctx = SSL_CTX_new(TLS_method());
    if (dtls.ctx == NULL) {
        ERR_print_errors_fp(stderr);
        throw_error("cannot create SSL_CTX.\n");
    }
    

    // Set our supported ciphers
    int result = SSL_CTX_set_cipher_list(dtls.ctx, "ALL:!ADH:!LOW:!EXP:!MD5:@STRENGTH");
    if (result != 1) {
        ERR_print_errors_fp(stderr);
        throw_error("cannot set the cipher list.\n");
    }

    SSL_CTX_set_verify(dtls.ctx, SSL_VERIFY_PEER, verify_callback);

    SSL_CTX_set_mode(dtls.ctx, SSL_MODE_ENABLE_PARTIAL_WRITE | SSL_MODE_ACCEPT_MOVING_WRITE_BUFFER);
}


void ssl_stream::setup_certificate(const std::vector<uint8_t>& certificate_base64) {
    // Load the certificate file; contains also the public key
    const std::vector<uint8_t> certificate_asn1 = decode_base64(certificate_base64);
    if (certificate_asn1.size() == 0) {
        throw_error("no certificate has been provided");
    }
    int result = SSL_CTX_use_certificate_ASN1(dtls.ctx, certificate_asn1.size(), certificate_asn1.data());
    if (result != 1) {
        ERR_print_errors_fp(stderr);
        throw_error("cannot load certificate.");
    }
}

void ssl_stream::setup_private_key(const std::vector<uint8_t>& private_key_base64)
{
    const std::vector<uint8_t> private_key_asn1 = decode_base64(private_key_base64);
    if (private_key_asn1.size() == 0) {
        throw_error("no private key has been provided");
    }

    // Load private key
    int result = SSL_CTX_use_PrivateKey_ASN1(EVP_PKEY_RSA, dtls.ctx, private_key_asn1.data(), private_key_asn1.size());
    if (result != 1) {
        ERR_print_errors_fp(stderr);
        log_traffic(lg, spdlog::level::err, "can not load private key: ", private_key_asn1.data(), private_key_asn1.size());
        throw_error("cannot load private key");
    }

    // Check if the private key is valid
    result = SSL_CTX_check_private_key(dtls.ctx);
    if (result != 1) {
        ERR_print_errors_fp(stderr);
        throw_error("checking the private key failed");
    }
}

void ssl_stream::init()
{
    dtls.bio = BIO_new_ssl_connect(dtls.ctx);
    if (dtls.bio == NULL) {
        throw_error("error connecting to server\n");
    }

    BIO_get_ssl(dtls.bio, &(dtls.ssl));
    if (dtls.ssl == NULL) {
        throw_error("BIO_get_ssl failed\n");
    }
}



ssl_stream::ssl_stream(int s_fd, const std::vector<uint8_t>& certificate_base64, const std::vector<uint8_t>& private_key_base64, const std::vector<uint8_t>& peer_certificate_fingerprint_base64)
    : lg("ssl_stream")
{
    socket_fd = s_fd;
    SSL_library_init();
    init_context(peer_certificate_fingerprint_base64);
	setup_certificate(certificate_base64);
	setup_private_key(private_key_base64);
    init();
    //SSL_set_mode(dtls.ssl, SSL_MODE_AUTO_RETRY);

    SSL_set_fd(dtls.ssl, s_fd);
}

void ssl_stream::set_accept_state() {
    SSL_set_accept_state(dtls.ssl);
}

void ssl_stream::set_connect_state() {
    SSL_set_connect_state(dtls.ssl);
}

void ssl_stream::handle_ssl_error_syscall(const char* operation, int ret) 
{
    int last_errno = errno;
    if  (ERR_peek_error() == 0) {
        //no errors in openssl error queue
        if (ret == 0) {
            throw_error("unexpected EOF on ssl socket on " + std::string(operation));
        } 
        if (ret == -1) {
            //socket io error
            if (last_errno == EAGAIN) {
                lg.info("EAGAIN observed on %s, it's ok\n", operation);
            } else {
                throw_system_error("socket io error on " +  std::string(operation));
            }
        }
        throw_error("should not get here " + std::string(operation) +" result < -1");
    } else {
        lg.error("ssl error syscall on %s, error = %lu, result = %d\n", operation, ERR_peek_error(), ret);
        ERR_print_errors_fp(stderr);
        throw_error("some io error on " + std::string(operation) + ", see log");
    }
}

int ssl_stream::read(uint8_t* ip_header_destination_start, size_t length) {
    lg.debug("ssl_stream::read begin, length = %lu\n", length);
    int result = SSL_read(dtls.ssl, ip_header_destination_start, length);
    if (result <= 0) {
        int ssl_ec = SSL_get_error(dtls.ssl, result);
        if (ssl_ec != SSL_ERROR_WANT_READ && ssl_ec != SSL_ERROR_WANT_WRITE && ssl_ec != SSL_ERROR_SYSCALL) {
            //log_error("SSL_read error, %s\n", ERR_error_string(ssl_ec, NULL));
            ERR_print_errors_fp(stderr);
            throw_error("SSL_read error");
            //exit(1);
        }
        if (ssl_ec == SSL_ERROR_WANT_READ) {
            lg.debug("ssl wants read");
        }
        if (ssl_ec == SSL_ERROR_WANT_WRITE) {
            lg.debug("ssl wants write");
        }
        if (ssl_ec == SSL_ERROR_SYSCALL) {
            handle_ssl_error_syscall("SSL_read", result); 
        }
        return -1;
    }
    if (result == 0) {
        throw_error("unimaginable on read\n");
    }

    lg.debug("SSL_read read %zu bytes", result);
    log_traffic(lg, spdlog::level::trace, "SSL_read traffic: ", ip_header_destination_start, result);

    return result;
}


int ssl_stream::write(uint8_t* data_start, size_t data_count) {
    if (data_count == 0) {
        throw_error("ssl_stream::write data_count == 0");
    }
    lg.debug("ssl_stream::write begin, length = %lu\n", data_count);
    int sent_count = SSL_write(dtls.ssl, data_start,  data_count);

    if (sent_count <= 0) {
        int ssl_ec = SSL_get_error(dtls.ssl, sent_count);
        if (ssl_ec != SSL_ERROR_WANT_READ && ssl_ec != SSL_ERROR_WANT_WRITE && ssl_ec != SSL_ERROR_SYSCALL) {
            //log_error("SSL_write error: %s\n", ERR_error_string(ssl_ec, NULL));
            ERR_print_errors_fp(stderr);
            throw_error("SSL_write error");
            //exit(1);
        }
        if (ssl_ec == SSL_ERROR_WANT_READ) {
            lg.debug("ssl wants read\n");
        }
        if (ssl_ec == SSL_ERROR_WANT_WRITE) {
            lg.debug("ssl wants write\n");
        }
        if (ssl_ec == SSL_ERROR_SYSCALL) {
            handle_ssl_error_syscall("SSL_write", sent_count);
        }
        lg.warn("FRAGMENT LOST\n");
        return -1;
    }
    if (sent_count == 0) {
        throw_error("unimaginable on write\n");
    }
    if (sent_count > 0) {
        lg.debug("SSL_write written %zu bytes", sent_count);
        log_traffic(lg, spdlog::level::trace, "SSL_write traffic: ", data_start, sent_count);
    }
    return sent_count;
}


