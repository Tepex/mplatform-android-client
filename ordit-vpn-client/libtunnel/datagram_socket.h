#ifndef __DATAGRAM_SOCKET_H
#define __DATAGRAM_SOCKET_H

#include <cstdint>
#include <cstddef>

struct datagram_socket {
    virtual int receive(uint8_t* buffer, size_t buffer_size) = 0;
    virtual int send(uint8_t* buffer, size_t count) = 0;
    virtual int fd() const = 0;
};

#endif
