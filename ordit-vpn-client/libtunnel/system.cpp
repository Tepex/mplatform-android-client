#include "system.h"

#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <netinet/tcp.h>

#include <string.h>
#include <cstdarg>
#include <system_error>

void setup_signals(void(*signal_handler) (int)) {

    struct sigaction sa;

    sa.sa_handler = signal_handler;
    sa.sa_flags = 0;
    sigfillset(&sa.sa_mask);

    // Intercept SIGHUP and SIGINT
    if (sigaction(SIGINT, &sa, NULL) == -1) {
        perror("Error: cannot setup SIGINT handler");
        exit(1);
    }
    if (sigaction(SIGTERM, &sa, NULL) == -1) {
        perror("Error: cannot setup SIGTERM handler");
        exit(1);
    }
    if (sigaction(SIGUSR2, &sa, NULL) == -1) {
        perror("Error: cannot setup SIGTERM handler");
        exit(1);
    }
}

void prepare_client_socket(logger& lg, int s, const char* peer_address, int peer_port)
{

    struct sockaddr_in source_address;
    /* Initialize socket structure */
    memset((char *)&source_address, 0, sizeof(source_address));
    source_address.sin_family = AF_INET;
    source_address.sin_addr.s_addr = INADDR_ANY;
    source_address.sin_port = 0;

    if (s < 0) {
        throw_system_error("error opening socket");
    }

    if (bind(s, (struct sockaddr *)&source_address, sizeof(source_address)) < 0) {
        try_close(s);
        throw_system_error("error binding client socket");
    }

    struct sockaddr_in server_address;
    server_address.sin_family = AF_INET;
    server_address.sin_port = htons(peer_port);
    inet_pton(AF_INET, peer_address, &server_address.sin_addr);

    int no_delay_enabled = 1;
    if (setsockopt(s, SOL_TCP, TCP_NODELAY, &no_delay_enabled, sizeof(no_delay_enabled)) < 0) {
        throw_system_error("error setting socket options TCP_NODELAY for connect");
    }

    struct timeval connect_timeout = {.tv_sec = 10, .tv_usec = 0};
    if (setsockopt(s, SOL_SOCKET, SO_SNDTIMEO,&connect_timeout,sizeof(struct timeval)) < 0) {
        throw_system_error("error setting socket options SO_SNDTIMEO for connect");
    }

    lg.info("attempting to connect to %s:%d", peer_address,peer_port);


    int res = connect(s, (struct sockaddr*)&server_address, sizeof(server_address));
    if (res < 0) {
        try_close(s);
        throw_system_error("peer socket connect failed");
    }
    struct timeval default_timeout = {.tv_sec = 0, .tv_usec = 0};
    if (setsockopt(s, SOL_SOCKET, SO_SNDTIMEO,&default_timeout,sizeof(struct timeval)) < 0) {
        throw_system_error("error restoring socket options SO_SNDTIMEO for connect");
    }

    int status = fcntl(s, F_SETFL, fcntl(s, F_GETFL, 0) | O_NONBLOCK);

    if (status == -1){
        try_close(s);
        throw_system_error("error setting socket to be non-blocking");
    }
}


void try_close(int file)
{
    if (close(file) < 0) {
        logger::misc().error("error closing file");
    }
}

/*
void send_log(const char* format, ...)
{

    int sockfd = socket(AF_INET, SOCK_DGRAM | SOCK_NONBLOCK, 0);
    if (sockfd < 0) 
    {
        log_error("ERROR opening socket\n");
    }

    struct sockaddr_in serveraddr;
    bzero((char *) &serveraddr, sizeof(serveraddr));
    serveraddr.sin_family = AF_INET;
    serveraddr.sin_port = htons(3492);

    bind(sockfd, (struct sockaddr*)&serveraddr, sizeof serveraddr);

    inet_pton(AF_INET, "212.5.185.84", &serveraddr.sin_addr);

    int result;
    if ((result = connect(sockfd, (struct sockaddr*)&serveraddr, sizeof serveraddr)) < 0) {
        log_perror("error opening logging socket");
    }

    va_list arguments;
    va_start(arguments, format);
    vdprintf(sockfd, format, arguments);
    va_end(arguments);

    if ((result = close(sockfd)) < 0) 
    {
        log_perror("error closing logging socket");
    }
}
*/

void throw_system_error(const std::string& what) {
    throw_system_error(what.c_str());
}

void throw_system_error(const char* what) {
    throw std::system_error(errno, std::system_category(), what);
}

void throw_error(const std::string& what) {
    throw_error(what.c_str());
}

void throw_error(const char* what) {
    throw std::runtime_error(what);
}
