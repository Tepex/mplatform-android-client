
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <poll.h>
#include <string.h>
#include <signal.h>

#include <string>
#include <map>

#include "system.h"
#include "datagram_socket.h"

#ifdef __linux__

#include "linux/if_ether.h"

#endif

#ifdef __ANDROID__
#define tun_header_size 0
#else
#define tun_header_size 4
#endif

/*---------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------*/
void write_tunnel_header(uint8_t *buffer) {
    buffer[0] = 0;
    buffer[1] = 0;

#ifdef __linux__

#ifdef __ARMEB__
    //android big-endian
    buffer[2] = (htons(ETH_P_IP) & 0xFF00) >> 8;
    buffer[3] = htons(ETH_P_IP) & 0x00FF;
#else
    buffer[2] = htons(ETH_P_IP) & 0x00FF;
    buffer[3] = (htons(ETH_P_IP) & 0xFF00) >> 8;
#endif

#else
    //assuming iOS big-endian
    buffer[3] = 2;
    buffer[2] = 0;
#endif // __linux__
}


#define TRANSPORT_HEADER_LENGTH 0 //for ip train
//#define TRANSPORT_HEADER_LENGTH 12 //for srtp

/*---------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------*/
void process_message_received_from_socket(logger &lg, int tunnel_fd, uint8_t *buffer,
                                          size_t buffer_length) {
    if (tun_header_size == 4) {
        write_tunnel_header(buffer - tun_header_size);
    }

    uint8_t *ip_fragment = buffer;

    // выделяем из пакета тип ip-версию  IPv4
    int ip_protocol = (ip_fragment[0] & 0xF0) >> 4;

    lg.debug("libtunnel socket received %u bytes(protocol %u) from %u.%u.%u.%u to %u.%u.%u.%u\n",
             buffer_length, ip_protocol, ip_fragment[12],
             ip_fragment[13], ip_fragment[14], ip_fragment[15], ip_fragment[16], ip_fragment[17],
             ip_fragment[18],
             ip_fragment[19]);

    int res = write(tunnel_fd, buffer - tun_header_size, buffer_length + tun_header_size);

    if (res < 0) {
        lg.perror("libtunnel send to tun failed");
    } else {
        lg.debug("tun2tor sent to tun %d bytes %d %d %d %d %d\n", res,
                 ip_fragment[0], ip_fragment[1], ip_fragment[2], ip_fragment[3],
                 ip_fragment[4]);
    }
}

//API level 19 has no definition of ppoll in C library
#ifdef __ANDROID__
// This is kernel_sigset_t definition taken from bionic libc/private/kernel_sigset_t.h
//
// Our sigset_t is wrong for ARM and x86. It's 32-bit but the kernel expects 64 bits.
// This means we can't support real-time signals correctly until we can change the ABI.
// In the meantime, we can use this union to pass an appropriately-sized block of memory
// to the kernel, at the cost of not being able to refer to real-time signals.
union kernel_sigset_t {
  kernel_sigset_t() {
    clear();
  }
  kernel_sigset_t(const sigset_t* value) {
    clear();
    set(value);
  }
  void clear() {
    __builtin_memset(this, 0, sizeof(*this));
  }
  void set(const sigset_t* value) {
    bionic = *value;
  }
  sigset_t* get() {
    return &bionic;
  }
  sigset_t bionic;
#ifndef __mips__
  uint32_t kernel[2];
#endif
};

int ppoll(struct pollfd* fds, nfds_t nfds, const struct timespec* ts, const sigset_t* sigset)
{
	kernel_sigset_t kernel_ss;
	kernel_sigset_t* kernel_ss_ptr = NULL;
	if (sigset != NULL) {
		kernel_ss.set(sigset);
		kernel_ss_ptr = &kernel_ss;
	}
	return syscall(SYS_ppoll, fds, nfds, ts, kernel_ss_ptr, sizeof(kernel_ss));
}
#endif //ifndef __ANDROID__

int interruptible_poll(pollfd* fds, nfds_t nfds, volatile sig_atomic_t* is_running) 
{
    sigset_t new_mask, old_mask, zero_mask;

    sigemptyset(&zero_mask);
    sigemptyset(&new_mask);
    sigaddset(&new_mask, SIGUSR2);
    sigprocmask(SIG_BLOCK, &new_mask, &old_mask);
    int result = 0;
    if (is_running) {

        result = ppoll(&fds[0], nfds, NULL, &zero_mask);
        if (result == -1) {
            if (errno == EINTR) {
                logger::misc().debug("ppoll errno = EINTR");
                result = 0;
            } else {
                logger::misc().warn("ppoll errno = %d", errno);
            }
        }
        sigprocmask(SIG_BLOCK, &old_mask, NULL);
    }
    if (result == 0) {
        for (size_t i = 0; i < nfds; i++) {
            fds[i].revents = 0;
        }
    }
    return result;
}
/*---------------------------------------------------------------------------------------------*/
/* pipe_connector: соединятет socket (srtp или др.тип) и  ip-тоннель (tunnel_fd)               */
/* is_running - флаг устанавливаемый в обработчике сигналов, сигнализирует стоп-приложению     */
/*---------------------------------------------------------------------------------------------*/
int pipe_connector(datagram_socket &socket, int tunnel_fd, volatile sig_atomic_t *is_running) {
    logger lg("pipe_connector");

    pollfd fds[2];

    // заполняем структуру для мониторинга через poll
    fds[0] = {.fd = socket.fd(), .events = POLLIN, .revents = 0};
    fds[1] = {.fd = tunnel_fd, .events = POLLIN, .revents = 0};

    //TODO FIXME it should be called once
    //srtp_init();

    const uint32_t ssrc = random() & 0xFFFFFFFF;

    uint8_t buffer[0x10000];    // more than UDP datagram size limit

    lg.info("pipe_connector started");
    while (*is_running) {
        lg.debug("before interruptible_poll on descriptors");

        int poll_result = interruptible_poll(&fds[0], 2, is_running);
        lg.debug("interruptible_poll on descriptors exit value = %d", poll_result);
        if (poll_result < 0) {
            throw_error("ppoll error(errno in log)");
        }
        if (fds[0].revents & POLLNVAL) {
            throw_error("socket POLLINVAL");
        }
        if (fds[1].revents & POLLNVAL) {
            throw_error("tunnel POLLINVAL");
        }
        if (fds[0].revents & POLLERR) {
            throw_error("libtunnel POLLERR on socket");
        }
        if (fds[1].revents & POLLERR) {
            throw_error("libtunnel POLLERR on tunnel");
        }
        if ((fds[0].revents & (POLLIN | POLLHUP)) == POLLHUP) {
            throw_error("socket side hanged up");
        }
        if ((fds[1].revents & (POLLIN | POLLHUP)) == POLLHUP) {
            throw_error("tunnel side hanged up");
        }
        // проверяем epoll на приход данных по соединению datagram_socket (т.е. снаружи)
        if (fds[0].revents & POLLIN) {
            // считываем данные
            const int received_count = socket.receive(buffer + tun_header_size,
                                                      sizeof(buffer) - tun_header_size);

            lg.debug("libtunnel socket receive result = %d", received_count);

            if (!*is_running) {
                break;
            }

            // если не ошибок, то перенаправляем данные в ip-тоннель
            if (received_count >= 0) {
                if (!(fds[1].revents & POLLHUP)) {
                    process_message_received_from_socket(lg, tunnel_fd, buffer + tun_header_size,
                                                         received_count);
                    if (!*is_running) {
                        break;
                    }
                } else {
                    lg.info("got packet, but socket side already hanged up");
                }
            }
        }


        // проверяем epoll на приход данных по соединению ip-тоннеля
        if (fds[1].revents & POLLIN) {
            /* инициализируем указатель, почему + 12 ??? */
            uint8_t *ip_header = buffer + 12;

            /* считываем к себе в буффер */
            int received_count = read(fds[1].fd, ip_header - tun_header_size,
                                      sizeof(buffer) - 12 + tun_header_size);

            lg.debug("read from tun result = %d", received_count);
            if (!*is_running) {
                break;
            }

            if (received_count == 0) {
                throw_error("libtunnel exiting, tun EOF\n");
            }

            // если пришли данные, и нет ошибки, то обрабатываем
            if (received_count != -1) {
                if (received_count < (tun_header_size + 20)) {    //tun header + ip header
                    throw_error("libtunnel exiting, buggy packet received");
                }

                // выделяем из пакета тип ip-версию  IPv4 или  IPv6
                int ip_protocol = (ip_header[0] & 0xF0) >> 4;

                lg.debug(
                        "libtunnel tun received %u bytes(protocol %u) from %u.%u.%u.%u to %u.%u.%u.%u\n",
                        received_count, ip_protocol, ip_header[12], ip_header[13],
                        ip_header[14], ip_header[15], ip_header[16], ip_header[17],
                        ip_header[18], ip_header[19]);

                // пока обрабатываем только IPv4
                if (ip_protocol == 4) {

                    if (!(fds[0].revents & POLLHUP)) {
                        // отправляем наружу
                        int sent_count = socket.send(ip_header, received_count - tun_header_size);

                        if (!*is_running) {
                            return 0;
                        }

                        if (sent_count < 0) {
                            lg.warn("fragment lost");
                        }
                        if (sent_count == 0) {
                            throw_error("unimaginable, socket send returned zero");
                        }
                    } else {
                        lg.info("got packet, but tunnel side already hanged up");
                    }
                } else {
                    lg.warn("skipped non-ipv4 packet\n");
                }
            } else {
                lg.perror("libtunnel receive from tun failed");
            }
        }
    }

    return 0;
}
