#include <array>
#include "datagram_socket.h"

class ssl_stream;

struct tls_socket : public datagram_socket {

    ssl_stream& stream;

    std::array<uint8_t, 0x10000> inbound_buffer;
    size_t inbound_buffered_count;

    std::array<uint8_t, 0x10000> outbound_buffer;
    size_t outbound_buffered_position;

    logger lg;

    explicit tls_socket(ssl_stream& backend);
    int fd() const;


    int receive(uint8_t* buffer, size_t buffer_size);

    int send(uint8_t* buffer, size_t count);
};
