#include "tunnel.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <net/if.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/select.h>
#include <sys/time.h>
#include <errno.h>

#include "system.h"

#include "openssl.h"
#include "tls_socket.h"
#include "pipe_connector.h"

static volatile sig_atomic_t is_running = 1;

//thread, responsible for interfacing with connection
static pthread_t caller_tid = -1;

static logger lg("API");

static std::vector<uint8_t> local_certificate;
static std::vector<uint8_t> local_private_key;
static std::vector<uint8_t> peer_certificate_fingerprint;

int get_sdk_version() {
    int ret = -1;
    std::string command = "getprop ro.build.version.sdk";
    FILE *file = popen(command.c_str(), "r");
    if (file) {
        char output[100];
        if (std::fgets(output, sizeof(output), file) != nullptr)
            ret = std::atoi(output);

        pclose(file);
    }
    return ret;
}

static void make_pid_checks() {
    if (caller_tid == -1) {
        throw std::logic_error(
                "another libtunnel API function called before calling make_stream_socket");
    }
    if (caller_tid != pthread_self()) {
        throw std::logic_error(
                "libtunnel API function called in the thread different from thread in which make_stream_socket has been called");
    }
}

static bool try_to_set_parameter(const char *name, const char *value) {
    if (strcmp(name, "local_certificate") == 0) {
        local_certificate = std::vector<uint8_t>(value, value + strlen(value));
        return true;
    }
    if (strcmp(name, "local_private_key") == 0) {
        local_private_key = std::vector<uint8_t>(value, value + strlen(value));
        return true;
    }
    if (strcmp(name, "peer_certificate_fingerprint") == 0) {
        peer_certificate_fingerprint = std::vector<uint8_t>(value, value + strlen(value));
        return true;
    }
    if (strcmp(name, "loglevel") == 0) {
        set_log_level(loglevel_string_to_enum(value));
        return true;
    }
    return false;
}

void set_parameter(const char *name, const char *value) {
    lg.info("set_parameter called: name=%s value=%.20s..[may be truncated]", name, value);
    if (try_to_set_parameter(name, value)) {
        lg.info("parameter %s have been set to value %.20s..[may be truncated]", name, value);
    } else {
        lg.critical("failed to set parameter %s to value %s", name, value);
        throw std::runtime_error("failed to set parameter(see log for details)");
    }
}

static void stop_tunnel_signal_handler(int unused) {
#ifndef NDEBUG
    char message[] = "\nsignal handler called\n"
    write(1, message, sizeof(message));
#endif
    is_running = 0;
}

int make_stream_socket() {
    lg.info("make_stream_socket called");
    if (caller_tid != pthread_self()) {
        caller_tid = pthread_self();
        setup_signals(stop_tunnel_signal_handler);
    }

    int result = socket(AF_INET, SOCK_STREAM, 0);
    if (result < 0) {
        lg.error("failed to create stream socket, errno = ", errno);
    }
    lg.info("make_stream_socket exits");
    return result;
}

static void
setup_socket_option(int socket_fd, int option_level, int option_name, int option_value) {
    if (setsockopt(socket_fd, option_level, option_name, &option_value, sizeof(option_value)) < 0) {
        lg.error("setsockopt on option_name=%d failed", option_name);
        throw std::runtime_error("setsockopt failed");
    }
}

static void setup_keepalive_on_socket(int socket_fd) {
    setup_socket_option(socket_fd, SOL_SOCKET, SO_KEEPALIVE, 1);
    setup_socket_option(socket_fd, IPPROTO_TCP, TCP_KEEPIDLE, 19);
    setup_socket_option(socket_fd, IPPROTO_TCP, TCP_KEEPINTVL, 8);
    setup_socket_option(socket_fd, IPPROTO_TCP, TCP_KEEPCNT, 7);
}

void connect_stream_socket(int stream_socket_fd, const char *peer_address, int peer_port) {
    lg.info("connect_stream_socket called");
    make_pid_checks();

    is_running = 1;

    setup_keepalive_on_socket(stream_socket_fd);
    prepare_client_socket(lg, stream_socket_fd, peer_address, peer_port);
}

//TODO closing of stream_socket_fd
void start_tunnel(int tunnel_fd, int stream_socket_fd, const char *resource_directory) {
    lg.info("start_tunnel called");
    make_pid_checks();

//  srtp_socket socket(peer_address, 3490);
    ssl_stream stream(stream_socket_fd, local_certificate, local_private_key,
                      peer_certificate_fingerprint);

    stream.set_connect_state();

    tls_socket socket(stream);
    //srtp_socket socket(3);
    pipe_connector(socket, tunnel_fd, &is_running);
    //soc_fd = stream_socket_fd;
    lg.info("start_tunnel exits");
}

int start_server_tunnel(int tunnel_fd, int stream_socket_fd, const char *resource_directory) {
    try {
        lg.info("start_server_tunnel called");

        is_running = 1;
        caller_tid = pthread_self();
        setup_signals(stop_tunnel_signal_handler);

        setup_keepalive_on_socket(stream_socket_fd);
        //  srtp_socket socket(peer_address, 3490);
        {
            ssl_stream stream(stream_socket_fd, local_certificate, local_private_key,
                              peer_certificate_fingerprint);
            stream.set_accept_state();

            tls_socket socket(stream);
            //srtp_socket socket(3);
            pipe_connector(socket, tunnel_fd, &is_running);
        }
        try_close(stream_socket_fd);
        try_close(tunnel_fd);

        lg.info("start_server_tunnel exits");
        if (is_running == 1) {
            lg.critical("oops is_running");
            throw std::logic_error("oops is_running");
        }
    } catch (std::exception &exception) {
        lg.error((std::string(exception.what()) + "\n").c_str());
        is_running = 0;
        return 1;
    }
    return 0;
}

void stop_tunnel(int stream_socket_fd) {
    lg.info("stop_tunnel called");
    if (get_sdk_version() > 25) {
        if (stream_socket_fd != -1) {
            close(stream_socket_fd);
        }
    } else {
        if (caller_tid == -1) {
            throw std::logic_error("stop_tunnel called, but no socket has been made so far");
        }
        if (is_running) {
            if (!pthread_kill(caller_tid, 0)) {
                pthread_kill(caller_tid, SIGUSR2);
            }
        }
    }
    is_running = 0;
    lg.info("stop_tunnel exits");
}