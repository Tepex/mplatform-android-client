#include <openssl/ssl.h>
#include <arpa/inet.h>

#include "system.h"


typedef struct {
    SSL_CTX *ctx;
    SSL *ssl;
    BIO *bio;
} DTLSParams;

struct ssl_stream {
    DTLSParams dtls;
    int socket_fd;
    logger lg;

    ssl_stream(int fd, const std::vector<uint8_t>& certificate_base64, const std::vector<uint8_t>& private_key_base64, const std::vector<uint8_t>& peer_certificate_fingerprint_base64);

    void set_accept_state();
    void set_connect_state();

    int read(uint8_t* ip_header_destination_start, size_t length);
    int write(uint8_t* data_start, size_t data_count);

    private:
        void handle_ssl_error_syscall(const char* operation, int ret); 

        void init();
        void init_context(const std::vector<uint8_t>& peer_certificate_fingerprint_base64);
        void setup_private_key(const std::vector<uint8_t>& certificate_base64);
        void setup_certificate(const std::vector<uint8_t>& certificate_base64);
};
