#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <srtp/srtp.h>
#include <string>
#include <string.h>

#include "datagram_socket.h"
#include "system.h"

//TODO FIXME
// Set key to predetermined value
uint8_t srtp_master_key[30] = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
                   0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
                   0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17,
                   0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D};

void write_rtp_header(uint32_t ssrc, uint8_t* buffer) {
    static int seq_num = 0;
    seq_num = (seq_num + 1) % 65536;
    buffer[0] = 0x80;
    buffer[1] = 0x07; //PT
    buffer[2] = htons(seq_num) & 0x00FF;
    buffer[3] = (htons(seq_num) & 0xFF00) >> 8;

    buffer[4] = 0; //TODO timestamp
    buffer[5] = 0;
    buffer[6] = 0;
    buffer[7] = 0;

    uint32_t ssrc_nbo = htonl(ssrc);
    memcpy(buffer + 8, &ssrc_nbo, 4);
}

//#include "../common/tun.c"
srtp_t create_srtp_session(ssrc_t ssrc) {

    srtp_policy_t policy;
    memset(&policy, 0x0, sizeof(srtp_policy_t));


    // set policy to describe a policy for an SRTP stream
    crypto_policy_set_rtp_default(&policy.rtp);
    crypto_policy_set_rtcp_default(&policy.rtcp);
    policy.ssrc =  ssrc;
    policy.key  = srtp_master_key;
    policy.next = NULL;

    // allocate and initialize the SRTP session
    srtp_t session;
    srtp_create(&session, &policy);
    return session;
}


const char* libsrtp_error_code_to_string(err_status_t error_code)
{
    switch (error_code)
    {
        case err_status_ok: return "nothing to report";
        case err_status_fail: return "unspecified failure";
        case err_status_bad_param: return "unsupported parameter";
        case err_status_alloc_fail: return "couldn't allocate memory";
        case err_status_dealloc_fail: return "couldn't deallocate properly";
        case err_status_init_fail: return "couldn't initialize";
        case err_status_terminus: return "can't process as much data as requested";
        case err_status_auth_fail: return "authentication failure";
        case err_status_cipher_fail: return "cipher failure";
        case err_status_replay_fail: return "replay check failed (bad index)";
        case err_status_replay_old: return "replay check failed (index too old)";
        case err_status_algo_fail: return "algorithm failed test routine";
        case err_status_no_such_op: return "unsupported operation";
        case err_status_no_ctx: return "no appropriate context found";
        case err_status_cant_check: return "unable to perform desired validation";
        case err_status_key_expired: return "can't use key any more";
        case err_status_socket_err: return "error in use of socket";
        case err_status_signal_err: return "error in use POSIX signals";
        case err_status_nonce_bad: return "nonce check failed";
        case err_status_read_fail: return "couldn't read data";
        case err_status_write_fail: return "couldn't write data";
        case err_status_parse_err: return "error parsing data";
        case err_status_encode_err: return "error encoding data";
        case err_status_semaphore_err: return "error while using semaphores";
        case err_status_pfkey_err: return "error while using pfkey";
    }
}

void rtp_print_error(err_status_t error_code, const char *message) {
    log_error("error: %s %d ( %s )\n", message, error_code, libsrtp_error_code_to_string(error_code));
}


struct srtp_socket : public datagram_socket {

    srtp_t session_in;
    srtp_t session_out;
    uint32_t ssrc;
    struct sockaddr_in peer_endpoint;
    int socket_fd;


    void init_srtp() {
        //TODO should be initialized once
        srtp_init();

        ssrc = random() & 0xFFFFFFFF;

        session_in = create_srtp_session({ssrc_any_inbound,0});
        session_out = create_srtp_session({ssrc_specific, ssrc });
    }

    srtp_socket(int s_fd, const sockaddr_in & server_address) {
        prepare_client_socket(s_fd, server_address);
        socket_fd = s_fd;

        init_srtp();
    }

    srtp_socket(int fd/*, struct sockaddr_in& peer_addr*/) {
        this->socket_fd = fd;
        //this->peer_endpoint = peer_addr;
        init_srtp();
    }

    int receive(uint8_t* buffer, const size_t buffer_size) override {
        struct sockaddr_in peer_addr;
        socklen_t peer_addr_len = sizeof(peer_addr);
        int received_count =
            recvfrom(socket_fd, buffer, buffer_size, 0,
                     (struct sockaddr *)&peer_addr, &peer_addr_len);

        if (received_count < 0) {
            log_perror("read from socket failed");
            return -1;
        }
        if (sizeof(peer_addr) != peer_addr_len) {
            log_error("message of wrong protocol received\n");
            return -1;
        }

        /*
        if (peer_addr != peer_endpoint) {
            log_error("client received alien datagram\n");
            return -1;
        }
        */

        debug_printf("tun2tor_run received from port %d\n",
                     ntohs(peer_addr.sin_port));

        int srtp_len = received_count;
        err_status_t error_code = srtp_unprotect(session_in, buffer, &srtp_len);
        if (error_code != 0) {
            rtp_print_error(error_code, "srtp_protect failed");
            return -1;
        }
        debug_printf("srtp_unprotect success original length - %d, unprotected length - %d\n", received_count, srtp_len);
        
        return received_count;
    }

    int send(uint8_t* buffer, const size_t count) override {
        write_rtp_header(ssrc, buffer - 12);
        int srtp_len = count + 12;
        err_status_t error_code = srtp_protect(session_out, buffer - 12, &srtp_len);

        if (error_code != 0) {
            rtp_print_error(error_code, "srtp_protect failed");
            return -1;
        }
        debug_printf("srtp_protect success original length - %zu, protected length - %d\n", count, srtp_len);

        return ::send(socket_fd, buffer - 12, srtp_len, 0);
    }

    int fd() const override{
        return socket_fd;
    }

};
