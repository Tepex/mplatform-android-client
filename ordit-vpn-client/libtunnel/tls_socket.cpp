#include <assert.h>
#include <string.h>

#include "openssl.h"

#include "tls_socket.h"
#include "system.h"

tls_socket::tls_socket(ssl_stream& backend) :
        stream(backend)
        ,lg("tls_socket") {

    inbound_buffered_count = 0;
    outbound_buffered_position = outbound_buffer.size();
}

int tls_socket::fd() const {
    return stream.socket_fd;
}

int tls_socket::receive(uint8_t* buffer, size_t buffer_size) {
    if (inbound_buffered_count < 4) {
        //TODO rewrite algorithm to work without multiple limited buffer reads(use ring buffer)
        int read_count = stream.read(inbound_buffer.data() + inbound_buffered_count, 4 - inbound_buffered_count);
        if (read_count < 0) {
            return -1;
        }
        inbound_buffered_count += read_count;

    }
    if (inbound_buffered_count >= 4) {
        size_t ip_packet_size = (inbound_buffer[2] << 8) | inbound_buffer[3];
        if (ip_packet_size > buffer_size) {
            throw_error("too small buffer provided, should not happen");
        }

        int read_count = stream.read(inbound_buffer.data() + inbound_buffered_count, ip_packet_size - inbound_buffered_count);
        if (read_count < 0) {
            return -1;
        }
        inbound_buffered_count += read_count;

        assert(inbound_buffered_count <= ip_packet_size);
        if (inbound_buffered_count == ip_packet_size) {
            //TODO rewrite interface to work without memcpy
            memcpy(buffer, inbound_buffer.data(), ip_packet_size);
            inbound_buffered_count = 0;
            return ip_packet_size;
        }
    }
    return - 1;
}

int tls_socket::send(uint8_t* buffer, size_t count) {

    if (count > outbound_buffer.size()) {
        lg.error("packet too big to fit in udp datagram");
        return -1;
    }
    if (outbound_buffered_position < outbound_buffer.size()) {
        int result = stream.write(outbound_buffer.data() + outbound_buffered_position, outbound_buffer.size() - outbound_buffered_position);
        if (result < 0) {
            return -1;
        }
        outbound_buffered_position += result;
    }
    if (outbound_buffered_position == outbound_buffer.size()) {
        int result = stream.write(buffer, count);
        int write_count = result >= 0 ? result : 0;
        outbound_buffered_position = outbound_buffer.size() - count + write_count;
        memcpy(outbound_buffer.data() + outbound_buffered_position, buffer + write_count, count - write_count);
        //TODO leftovers may happen to be stuck in the outbound_buffer
        return count;
    }
    return -1;
}
