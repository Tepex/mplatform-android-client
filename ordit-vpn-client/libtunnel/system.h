#pragma once
#include <arpa/inet.h>
#include <signal.h>
#include <errno.h>
#include <string>
#include <logger.h>

void setup_signals(void(*signal_handler) (int));

void setup_ambient_capabilities();
void prepare_client_socket(logger& lg, int s, const char* peer_address, int peer_port);
void try_close(int file);
//void error(const char *msg);

void throw_system_error(const std::string& what);
void throw_system_error(const char* what);
void throw_error(const std::string& what);
void throw_error(const char* what);

