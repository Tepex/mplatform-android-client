package com.ordit.vpn.service

import android.app.Service
import android.content.Intent
import android.net.VpnService
import android.os.ParcelFileDescriptor
import android.support.v4.content.LocalBroadcastManager
import java.io.IOException
import java.util.*
import java.util.logging.Level
import java.util.logging.Logger

enum class State {
    CONNECTING,
    CONNECTED,
    DISCONNECTING,
    DISABLED
}

enum class Error {
    NO_ERROR,
    UNREACHABLE,
    TUN_SETUP_FAILED,
    GENERIC_ERROR
}

class OrditVpnService : VpnService(), Runnable {

    val log = Logger.getLogger(OrditVpnService::class.java.name)!!

    private var address: String = ""
    private var port: Int = -1

    companion object {
        init {
            System.loadLibrary("native-client")
        }

        private val INTERFACE_ADDRESS = "10.20.254.254"
        private val INTERFACE_ROUTE = "0.0.0.0"
    }

    private external fun makeStreamSocket(): Int
    private external fun connect(streamSocketFD: Int, peerAddress: String, peerPort: Int): String
    private external fun start(tun: Int, streamSocketFD: Int): String
    private external fun stop(streamSocketFD: Int)
    private external fun setParameter(name: String, value: String)

    private lateinit var vpnThread: Thread

    private var configure: ParcelFileDescriptor? = null
    private var streamSocketFD: Int = -1

    override fun onCreate() {
        vpnThread = Thread(this)
        val properties = Properties()
        try {
            applicationContext.assets.open("libtunnel.properties").use { inputStream ->
                properties.load(inputStream)
            }
        } catch (e: IOException) {
            log.log(Level.SEVERE, "FAIL_TO_READ_CONFIG_FILE", e)
            System.exit(99)
        }
        properties.forEach({ setParameter(it.key.toString(), it.value.toString()) })
    }

    override fun onTaskRemoved(rootIntent: Intent) {
        super.onTaskRemoved(rootIntent)
        log.log(Level.INFO, "TASK_REMOVED")
        closeTun()
    }

    override fun onDestroy() {
        log.log(Level.INFO, "SERVICE_DESTROYED")
        closeTun()
        setState(State.DISABLED)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val extras = intent?.extras
        if (extras != null) {
            if (vpnThread.state == Thread.State.TERMINATED) {
                vpnThread = Thread(this)
            }
            address = extras.getString("server", address)
            port = extras.getInt("port", port)
            if (vpnThread.state == Thread.State.NEW) {
                vpnThread.start()
            }
        } else {
            log.log(Level.INFO, "STOP_TUNNEL")
            closeTun()
        }
        return Service.START_NOT_STICKY
    }

    private fun configure(): ParcelFileDescriptor? {
        return try {
            val builder = Builder()
            builder.setMtu(1460)
            builder.addDnsServer("8.8.8.8")
            builder.addAddress(INTERFACE_ADDRESS, 24)
            builder.addRoute(INTERFACE_ROUTE, 0)
            builder.setConfigureIntent(null)
            builder.setSession("Munity")
            val vpnInterface = builder.establish()
            if (vpnInterface != null) {
                log.log(Level.INFO, "fd=${vpnInterface.fd}")
            }
            vpnInterface
        } catch (e: Exception) {
            log.log(Level.SEVERE, "TUN_SETUP_FAILED", e)
            null
        }
    }

    override fun run() {
        try {
            setState(State.CONNECTING)
            log.log(Level.INFO, "connect to $address:$port")
            streamSocketFD = makeStreamSocket()
            protect(streamSocketFD)
            val connectCode = connect(streamSocketFD, address, port)
            if (connectCode.isNotBlank()) {
                log.log(Level.WARNING, "unable to connect, error code is: $connectCode")
                setState(State.DISABLED)
                if (!connectCode.contains("Interrupted")) { // TODO: make exit codes
                    log.log(Level.WARNING, "server is unreachable: $address:$port")
                    setError(Error.UNREACHABLE)
                }
                return
            } else log.log(Level.WARNING, "CONNECTION_ESTABLISHED")
            configure = configure()
            if (configure == null) {
                log.log(Level.SEVERE, "CONFIGURE_IS_NULL")
                setState(State.DISABLED)
                setError(Error.TUN_SETUP_FAILED)
                return
            }
            setState(State.CONNECTED)
            val closeCode = start(configure?.fd!!, streamSocketFD)
            if (closeCode.isBlank()) {
                log.log(Level.INFO, "closed successfully")
            } else {
                log.log(Level.SEVERE, "closed with code: $closeCode")
            }
        } catch (e: Exception) {
            log.log(Level.SEVERE, "Error", e)
            setError(Error.GENERIC_ERROR)
        }
    }

    private fun closeTun() {
        log.log(Level.INFO, "closed tun called")
        setState(State.DISCONNECTING)
        stop(streamSocketFD)
        configure?.close().also { configure = null }
        setState(State.DISABLED)
    }

    private fun setState(state: State) {
        val intent = Intent("com.ordit.vpn")
        intent.putExtra("state", state)
        LocalBroadcastManager.getInstance(this.applicationContext).sendBroadcast(intent)
    }

    private fun setError(error: Error) {
        val intent = Intent("com.ordit.vpn")
        intent.putExtra("error", error)
        LocalBroadcastManager.getInstance(this.applicationContext).sendBroadcast(intent)
    }
}