package com.ordit.vpn

import android.app.Dialog
import android.content.SharedPreferences
import android.support.v4.content.LocalBroadcastManager
import com.alohamobile.vpnclient.*
import com.ordit.vpn.client.OrditVpnClient

/**
 * Created by Ivan on 23.11.2017.
 */

class OrditVpnProvider(private val consumer: VpnConsumer,
                       private val preferences: SharedPreferences,
                       private val loader: VpnClientConfigurationLoader,
                       private val vpnLogService: VpnLogService,
                       dialog: Dialog) : VpnProvider, VpnClientEvents {

    private var vpnClient: VpnClient = OrditVpnClient(
            LocalBroadcastManager.getInstance(consumer.context),
            dialog
    )

    init {
        vpnClient.setVpnEvents(this)
    }

    override fun start() {
        loader.load({
            if (it == null) {
                consumer.onVpnError()
            } else {
                isNeedReconnect = true
                onConfigurationLoaded(it)
            }
        })
    }

    private fun onConfigurationLoaded(configuration: VpnClientConfiguration) {
        try {
            vpnClient.initStart(configuration, consumer.context)
        } catch (ex: Exception) {
            consumer.onVpnError()
            vpnLogService.logError(ex.message, vpnClient.logs, client().isShutdown, client().isInitialized)
        } finally {
            loader.clearResources()
        }
    }

    override fun stop() {
        if (VpnProvider.holder.isConnected) {
            vpnClient.disconnect(consumer.context)
        }
        isNeedReconnect = false
        VpnProvider.holder.isConnected = false
    }

    override fun setNeedReconnect(isReconnect: Boolean) {
        preferences.edit().putBoolean(VpnProvider.holder.NEED_RECONNECT_VPN, isReconnect).apply()
    }

    override fun isNeedReconnect(): Boolean = preferences.getBoolean(VpnProvider.holder.NEED_RECONNECT_VPN, false)

    override fun onActivityResult(requestCode: Int, resultCode: Int): Boolean {
        return if (requestCode == VpnClient.PREPARE_VPN_SERVICE) {
            if (resultCode == -1) {
                vpnClient.completeStart(consumer.context)
            }
            if (resultCode == 0) {
                consumer.onOpenVpnDisconnected()
            }
            true
        } else false
    }

    override fun clear() {
        isNeedReconnect = VpnProvider.holder.isConnected
        vpnClient.shutdown(consumer.context)
        VpnProvider.holder.isConnected = false
        consumer.onOpenVpnDisconnected()
    }

    override fun onStateChange(state: VpnClientState) {
        when (state) {
            VpnClientState.DISABLED -> {
                consumer.onOpenVpnDisconnected()
                VpnProvider.holder.isConnected = false
            }
            VpnClientState.CONNECTING -> {
                consumer.startVpnConnection()
            }
            VpnClientState.CONNECTED -> {
                consumer.onOpenVpnConnected()
                VpnProvider.holder.isConnected = true
            }
            VpnClientState.DISCONNECTING -> {

            }
        }
    }

    override fun onPortChanged(oldPort: Int?, newPort: Int, errorState: VpnClientError) {
        vpnLogService.logError(errorState.toString(), vpnClient.logs, client().isShutdown, client().isInitialized)
    }

    override fun onHostChanged(oldHost: String, newHost: String, errorState: VpnClientError) {
        vpnLogService.logError(errorState.toString(), vpnClient.logs, client().isShutdown, client().isInitialized)
    }

    override fun onError(error: VpnClientError) {
        when (error) {
            VpnClientError.UNREACHABLE -> start() // get new config if server UNREACHABLE only
            VpnClientError.VPN_ERROR_FAILED -> consumer.onVpnError()
            else -> vpnLogService.logError(error.toString(), vpnClient.logs, client().isShutdown, client().isInitialized)
        }
    }

    private fun client(): OrditVpnClient = this.vpnClient as OrditVpnClient
}