package com.ordit.vpn.loging

import android.util.Log
import com.ordit.vpn.BuildConfig
import com.ordit.vpn.transport.OrditTransportService
import java.util.*
import java.util.logging.Handler
import java.util.logging.Level
import java.util.logging.LogManager
import java.util.logging.LogRecord

/**
 * Created by Ivan on 25.12.2017.
 */

class OrditLoggingHandler(val transport: OrditTransportService) : Handler() {

    override fun close() {}

    override fun flush() {}

    override fun publish(record: LogRecord) {
        if (!super.isLoggable(record))
            return

        val name = record.loggerName
        val maxLength = 30
        val tag = if (name.length > maxLength) name.substring(name.length - maxLength) else name

        try {
            val level = getAndroidLevel(record.level)
            Log.println(level, tag, record.message)
            if (record.thrown != null) {
                Log.println(level, tag, Log.getStackTraceString(record.thrown))
            }
        } catch (e: RuntimeException) {
            Log.e("OrditLoggingHandler", "Error logging message.", e)
        }

        val trace = Thread.currentThread().stackTrace
        tracer@ for ((index, line) in trace.withIndex()) {
            if (line.className == record.sourceClassName) {
                transport.sendLog(com.ordit.vpn.transport.entity.LogRecord(
                        packageName = BuildConfig.APPLICATION_ID,
                        versionName = BuildConfig.VERSION_NAME,
                        versionCode = BuildConfig.VERSION_CODE,
                        instanceId = transport.instanceId,
                        date = Date(record.millis),
                        sourceFile = trace[index].className,
                        lineNumber = trace[index].lineNumber,
                        message = record.message,
                        stackTrace = Log.getStackTraceString(record.thrown),
                        level = record.level?.name),
                        object : OrditTransportService.DataCallbackAsync<Void> {
                            override fun onDataReceived(item: Void) {
                            }

                            override fun onError(message: String) {
                                Log.e("OrditLoggingHandler", "Error save log: $message")
                            }
                        })
                break@tracer
            }
        }
    }

    private fun getAndroidLevel(level: Level): Int {
        val value = level.intValue()
        return when {
            value >= 1000 -> Log.ERROR
            value >= 900 -> Log.WARN
            value >= 800 -> Log.INFO
            else -> Log.DEBUG
        }
    }

    companion object {
        fun reset(rootHandler: Handler) {
            val rootLogger = LogManager.getLogManager().getLogger("")
            val handlers = rootLogger.handlers
            for (handler in handlers) {
                rootLogger.removeHandler(handler)
            }
            rootLogger.addHandler(rootHandler)
        }
    }
}