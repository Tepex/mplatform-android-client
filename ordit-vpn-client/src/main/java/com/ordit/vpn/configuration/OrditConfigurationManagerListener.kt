package com.ordit.vpn.configuration

import android.os.Build
import com.alohamobile.vpnclient.VpnClientConfiguration
import com.alohamobile.vpnclient.VpnClientConfigurationLoader
import com.ordit.vpn.loging.OrditLoggingHandler
import com.ordit.vpn.transport.OrditTransportService
import com.ordit.vpn.transport.entity.Server
import java.util.*
import java.util.logging.Level
import java.util.logging.Logger

/**
 * Created by Ivan on 24.11.2017.
 */
class OrditConfigurationManager(val transport: OrditTransportService) : VpnClientConfigurationLoader {

    val log = Logger.getLogger(OrditConfigurationManager::class.java.name)!!

    private val def = Server()

    init {
        OrditLoggingHandler.reset(OrditLoggingHandler(transport))
        java.util.logging.Logger.getLogger("com.ordit.vpn").level = Level.INFO
        log.info("Device: [${Build.MANUFACTURER}/${Build.BRAND}/${Build.MODEL}/${Build.DEVICE}/${Build.VERSION.RELEASE}]")
    }

    override fun load(listener: VpnClientConfigurationLoader.OnConfigurationLoadListener) {
        log.log(Level.INFO, "start load config")
        transport.getServer(object : OrditTransportService.DataCallbackAsync<Server> {
            override fun onDataReceived(item: Server) {
                log.log(Level.INFO, "config loaded: $item")
                listener.onLoaded(makeConfiguration(item.address, item.port))
            }

            override fun onError(message: String) {
                log.log(Level.WARNING, "Error load config: $message")
                listener.onLoaded(makeConfiguration(def.address, def.port))
            }
        }, 10000, def)
    }

    private fun makeConfiguration(address: String, port: Int) = VpnClientConfiguration("".byteInputStream(),
            UUID.randomUUID().toString(),
            "",
            "",
            listOf(port),
            listOf(address))

    override fun clearResources() {
        /* do nothing */
    }
}