package com.ordit.vpn.client

import android.app.Activity
import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.VpnService
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import com.alohamobile.vpnclient.*
import com.ordit.vpn.service.Error
import com.ordit.vpn.service.OrditVpnService
import com.ordit.vpn.service.State

/**
 * Created by Ivan on 24.11.2017.
 */

class OrditVpnClient(broadcastManager: LocalBroadcastManager, val dialog: Dialog) : VpnClient {

    private lateinit var configuration: VpnClientConfiguration

    var isShutdown: Boolean = false
    var isInitialized: Boolean = false

    private lateinit var events: VpnClientEvents

    private var vpnEventsBroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val state = intent.getSerializableExtra("state")
            if (state != null) {
                val vpnClientState = convertFromState(state as State)
                if (vpnClientState != VpnClientState.DISCONNECTING) {
                    events.onStateChange(vpnClientState)
                }
            } else {
                val error = intent.getSerializableExtra("error")
                if (error != null) {
                    events.onError(convertFromError(error as Error))
                }
            }
        }
    }

    private fun convertFromError(error: Error): VpnClientError {
        return when (error) {
            Error.TUN_SETUP_FAILED -> VpnClientError.TUN_SETUP_FAILED
            Error.UNREACHABLE -> VpnClientError.UNREACHABLE
            else -> VpnClientError.GENERIC_ERROR
        }
    }

    private fun convertFromState(state: State): VpnClientState {
        return when (state) {
            State.CONNECTING -> VpnClientState.CONNECTING
            State.CONNECTED -> VpnClientState.CONNECTED
            State.DISCONNECTING -> VpnClientState.DISCONNECTING
            else -> VpnClientState.DISABLED
        }
    }

    init {
        broadcastManager.unregisterReceiver(vpnEventsBroadcastReceiver)
        broadcastManager.registerReceiver(vpnEventsBroadcastReceiver, IntentFilter("com.ordit.vpn"))
    }

    override fun shutdown(context: Context) {
        //LocalBroadcastManager.getInstance(context).unregisterReceiver(vpnEventsBroadcastReceiver)
        this.disconnect(context)
        this.isInitialized = false
        this.isShutdown = true
    }

    override fun initStart(configuration: VpnClientConfiguration, activity: Activity) {
        this.configuration = configuration
        val intent = VpnService.prepare(activity)
        if (intent != null) {
            dialog.setOnDismissListener {
                activity.startActivityForResult(intent, VpnClient.PREPARE_VPN_SERVICE)
            }
            dialog.show()
        } else {
            completeStart(activity.applicationContext)
        }
    }

    override fun completeStart(context: Context) {
        val intent = Intent(context, OrditVpnService::class.java)
        val config = Bundle()
        config.putString("server", configuration.hosts[0])
        config.putInt("port", configuration.ports[0])
        intent.putExtras(config)

        this.isInitialized = true
        context.startService(intent)
    }

    override fun getLogs(): String {
        return ""
    }

    override fun setVpnEvents(events: VpnClientEvents) {
        this.events = events
    }

    override fun disconnect(context: Context) {
        isInitialized = false
        context.startService(Intent(context, OrditVpnService::class.java))
    }
}