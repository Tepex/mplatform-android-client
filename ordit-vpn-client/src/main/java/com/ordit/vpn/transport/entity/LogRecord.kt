package com.ordit.vpn.transport.entity

import android.support.annotation.Keep
import java.util.*

/**
 * Created by Ivan on 22.12.2017.
 */

@Keep
data class LogRecord(
        var packageName: String? = null,
        var versionName: String? = null,
        var versionCode: Int? = null,
        var sourceFile: String? = null,
        var lineNumber: Int? = null,
        var message: String? = null,
        var stackTrace: String? = null,
        var date: Date? = null,
        var instanceId: String? = null,
        var level: String? = null
)