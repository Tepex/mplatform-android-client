package com.ordit.vpn.transport.firebase

import com.google.firebase.FirebaseApp
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase


/**
 * Created by Ivan on 15.12.2017.
 */

object FirebaseHelper {
    //val auth: FirebaseAuth = FirebaseAuth.getInstance()
    val database: DatabaseReference
        get() {
            val secondary = FirebaseApp.getInstance("ordit")
            return FirebaseDatabase.getInstance(secondary).reference
        }
}