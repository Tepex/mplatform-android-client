package com.ordit.vpn.transport

import com.ordit.vpn.transport.entity.Config
import com.ordit.vpn.transport.entity.LogRecord
import com.ordit.vpn.transport.entity.Server

/**
 * Created by Ivan on 15.12.2017.
 */

interface OrditTransportService {

    val instanceId: String

    interface DataCallbackAsync<in Entity> {
        fun onDataReceived(item: Entity)
        fun onError(message: String)
    }

    fun getServer(callback: DataCallbackAsync<Server>, timeout: Int, defaultServer: Server)
    fun getConfig(callback: DataCallbackAsync<Config?>)
    fun getVersion(callback: DataCallbackAsync<String>)
    fun sendLog(logRecord: LogRecord, callback: DataCallbackAsync<Void>)
}