package com.ordit.vpn.transport.entity

import android.support.annotation.Keep

/**
 * Created by Ivan on 15.12.2017.
 */

@Keep
data class Server(val address: String = "88.208.39.96", val port: Int = 443)