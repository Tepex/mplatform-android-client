package com.ordit.vpn.transport.entity

import android.support.annotation.Keep

/**
 * Created by Ivan on 15.12.2017.
 */

@Keep
data class Request(var type: Type, var value: Value) {
    enum class Type {
        GET_SERVER
    }

    enum class Value {
        NEW_REQUEST,
        PROCESSING,
        DONE
    }
}