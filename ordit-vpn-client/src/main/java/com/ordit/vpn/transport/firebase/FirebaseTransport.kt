package com.ordit.vpn.transport.firebase

import android.content.Context
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.iid.FirebaseInstanceId
import com.ordit.vpn.transport.OrditTransportService
import com.ordit.vpn.transport.entity.Config
import com.ordit.vpn.transport.entity.LogRecord
import com.ordit.vpn.transport.entity.Request
import com.ordit.vpn.transport.entity.Server
import java.util.*
import java.util.concurrent.atomic.AtomicBoolean
import java.util.logging.Level
import java.util.logging.Logger

/**
 * Created by Ivan on 15.12.2017.
 */

var isInitialized = false

class FirebaseTransport(context: Context) : OrditTransportService {

    val log = Logger.getLogger(FirebaseTransport::class.java.name)!!

    override val instanceId: String
        get() {
            val secondary = FirebaseApp.getInstance("ordit")
            return FirebaseInstanceId.getInstance(secondary).token ?: UUID.randomUUID().toString()
        }

    companion object {
        val REQUESTS = "requests"
        val CONFIG = "config"
        val VERSION = "version"
        val LOGS = "logs"
    }

    init {
        if (!isInitialized) {
            val options = FirebaseOptions.Builder()
                    .setApplicationId("1:508293274487:android:109744a9fd0f1b8b") // Required for Analytics.
                    .setApiKey("AIzaSyBMjPIi6vkTCIa9-9HeoV-NctcvMcLrR1E") // Required for Auth.
                    .setDatabaseUrl("https://ordit-vpn.firebaseio.com") // Required for RTDB.
                    .build()
            FirebaseApp.initializeApp(context /* Context */, options, "ordit")
            isInitialized = true
        }
    }

    override fun getServer(callback: OrditTransportService.DataCallbackAsync<Server>, timeout: Int, defaultServer: Server) {
        val serverLoaded = AtomicBoolean(false)

        val reference = FirebaseHelper.database
                .child(REQUESTS)
                .child(instanceId)
                .child(Request.Type.GET_SERVER.name)

        val listener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (!dataSnapshot.hasChildren() || serverLoaded.get()) {
                    return
                }
                try {
                    dataSnapshot.getValue(Server::class.java)?.let {
                        log.log(Level.INFO, "response received: $it")
                        if (!serverLoaded.getAndSet(true)) {
                            log.log(Level.INFO, "response callback: $it")
                            callback.onDataReceived(it)
                        }
                    }
                } catch (ex: Exception) {
                    log.log(Level.SEVERE, "exception response processing", ex)
                    callback.onError(ex.localizedMessage)
                } finally {
                    reference.removeEventListener(this)
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                log.log(Level.SEVERE, "cancelled: $databaseError")
                if (!serverLoaded.getAndSet(true)) {
                    reference.removeEventListener(this)
                    callback.onError(databaseError.message)
                }
            }
        }

        reference.addValueEventListener(listener)

        reference.setValue(Request.Value.NEW_REQUEST).addOnCompleteListener {

            Timer().schedule(object : TimerTask() {
                override fun run() {
                    if (!serverLoaded.getAndSet(true)) {
                        reference.removeEventListener(listener)

                        val configLoaded = AtomicBoolean(false)

                        getConfig(object : OrditTransportService.DataCallbackAsync<Config?> {
                            override fun onDataReceived(item: Config?) {
                                log.log(Level.WARNING, "config received: $item")
                                if (!configLoaded.getAndSet(true)) {
                                    log.log(Level.WARNING, "config callback: $item")
                                    callback.onDataReceived(item?.server ?: defaultServer)
                                }
                            }

                            override fun onError(message: String) {
                                if (!configLoaded.getAndSet(true)) {
                                    log.log(Level.SEVERE, "receive config: $message")
                                    callback.onError(message)
                                }
                            }
                        })

                        Timer().schedule(object : TimerTask() {
                            override fun run() {
                                if (!configLoaded.getAndSet(true)) {
                                    log.log(Level.WARNING, "callback default")
                                    callback.onDataReceived(defaultServer)
                                }
                            }
                        }, timeout.toLong())
                    }
                }
            }, timeout.toLong())
        }
    }

    override fun getConfig(callback: OrditTransportService.DataCallbackAsync<Config?>) {
        FirebaseHelper.database.child(CONFIG)
                .addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        callback.onDataReceived(dataSnapshot.getValue<Config>(Config::class.java))
                    }

                    override fun onCancelled(databaseError: DatabaseError) {
                        callback.onError(databaseError.message)
                    }
                })
    }

    override fun getVersion(callback: OrditTransportService.DataCallbackAsync<String>) {
        FirebaseHelper.database.child(VERSION)
                .addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        callback.onDataReceived(dataSnapshot.getValue(Long::class.java).toString())
                    }

                    override fun onCancelled(databaseError: DatabaseError) {
                        callback.onError(databaseError.message)
                    }
                })
    }

    override fun sendLog(logRecord: LogRecord, callback: OrditTransportService.DataCallbackAsync<Void>) {
        FirebaseHelper.database.child(LOGS)
                .child(UUID.randomUUID().toString())
                .setValue(logRecord)
                .addOnFailureListener({ e -> callback.onError(e.localizedMessage) })
    }
}