package com.ordit.vpn.transport.entity

import android.support.annotation.Keep

/**
 * Created by Ivan on 15.12.2017.
 */

@Keep
data class Config(val server: Server? = null)