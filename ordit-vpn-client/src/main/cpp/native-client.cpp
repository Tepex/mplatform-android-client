#include <jni.h>
#include <string>
#include <iostream>
#include <sstream>

#include "tunnel.h"
#include <android/log.h>

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-parameter"

extern "C" JNIEXPORT int JNICALL
Java_com_ordit_vpn_service_OrditVpnService_makeStreamSocket(JNIEnv *env, jobject) {
    __android_log_print(ANDROID_LOG_INFO, "libtunnel", "make steam socket");
    try {
        return make_stream_socket();
    } catch (std::exception &exception) {
        __android_log_print(ANDROID_LOG_INFO, "libtunnel make_stream_socket", exception.what());
        return -1;
    }
}

extern "C" JNIEXPORT jstring JNICALL
Java_com_ordit_vpn_service_OrditVpnService_setParameter(JNIEnv *env, jobject, jstring name,
                                                        jstring value) {
    __android_log_print(ANDROID_LOG_INFO, "libtunnel", "set parameter");
    try {
        const char *name_c_string = env->GetStringUTFChars(name, JNI_FALSE);
        const char *value_c_string = env->GetStringUTFChars(value, JNI_FALSE);

        set_parameter(name_c_string, value_c_string);
        env->ReleaseStringUTFChars(name, name_c_string);
        env->ReleaseStringUTFChars(value, value_c_string);
    } catch (std::exception &exception) {
        __android_log_print(ANDROID_LOG_ERROR, "libtunnel set_parameter", exception.what());
        return env->NewStringUTF(exception.what());
    }
    return env->NewStringUTF("");
}

extern "C" JNIEXPORT jstring JNICALL
Java_com_ordit_vpn_service_OrditVpnService_connect(JNIEnv *env, jobject, int stream_socket_fd,
                                                   jstring peer_address, int peer_port) {
    __android_log_print(ANDROID_LOG_INFO, "libtunnel", "connect to server");
    try {
        const char *peer_address_str = env->GetStringUTFChars(peer_address, JNI_FALSE);
        //todo check server
        connect_stream_socket(stream_socket_fd, peer_address_str, peer_port);
        env->ReleaseStringUTFChars(peer_address, peer_address_str);
    } catch (std::exception &exception) {
        __android_log_print(ANDROID_LOG_ERROR, "libtunnel connect_stream_socket", exception.what());
        return env->NewStringUTF(exception.what());
    }
    return env->NewStringUTF("");
}

extern "C" JNIEXPORT jstring JNICALL
Java_com_ordit_vpn_service_OrditVpnService_start(JNIEnv *env, jobject, int tunnel_fd,
                                                 int stream_socket_fd) {
    __android_log_print(ANDROID_LOG_INFO, "libtunnel", "start tunnel");
    try {

        start_tunnel(tunnel_fd, stream_socket_fd, ".");

    } catch (std::exception &exception) {
        __android_log_print(ANDROID_LOG_ERROR, "libtunnel start_tunnel", exception.what());
        return env->NewStringUTF(exception.what());
    }
    return env->NewStringUTF("");
}

extern "C" JNIEXPORT void JNICALL
Java_com_ordit_vpn_service_OrditVpnService_stop(JNIEnv *env, jobject, int stream_socket_fd) {
    __android_log_print(ANDROID_LOG_INFO, "libtunnel", "stop tunnel");
    try {
        stop_tunnel(stream_socket_fd);
    } catch (std::exception &exception) {
        __android_log_print(ANDROID_LOG_ERROR, "libtunnel stop_tunnel", exception.what());
    }
}
#pragma clang diagnostic pop