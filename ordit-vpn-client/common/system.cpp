#include <system.h>
#include <tunnel.h>
#include <stdio.h>
#include <signal.h>
#include <system_error>
#include "system_linux.h"

void process_libtunnel_configuration_file(const char* filename)
{
    FILE* file = fopen(filename, "r");
    if (file == NULL) {
        throw std::system_error(errno, std::system_category(), "error opening configuration file");

    }
    char parameter_name[1024];
    char parameter_value[8096];
    int fscanf_result;
    while ((fscanf_result = fscanf(file, "%s = %s\n", parameter_name, parameter_value)) == 2) {
        set_parameter(parameter_name, parameter_value);
    }
    if (!feof(file)) {
        throw std::system_error(errno, std::system_category(), "fscanf returned an unexpected value: " + std::to_string(fscanf_result));
    }
}

