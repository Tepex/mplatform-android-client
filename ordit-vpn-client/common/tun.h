#ifndef __OVPN_TNL_H
#define __OVPN_TNL_H

#include <string>

int tun_alloc(const char *dev);
void configure_vpn_tunnel(int tun_fd, const std::string & interface_name, int association_id);

#endif
