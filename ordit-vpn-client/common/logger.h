#include <spdlog/logger.h>
//#define LOG_TO_SOCKET

#ifdef LOG_TO_SOCKET


#endif

spdlog::level::level_enum loglevel_string_to_enum(const char* value);

void set_log_level(spdlog::level::level_enum log_level);

class logger : public spdlog::logger
{
public:
    explicit logger(const char* name);

    void perror(const char* what)
    {
        error("%s: %s\n", what, strerror(errno));
    }

    static logger& misc() {
        static logger instance("misc");
        return instance;
    }
};

