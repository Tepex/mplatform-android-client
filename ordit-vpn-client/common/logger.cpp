#include "logger.h"

#include <spdlog/sinks/ansicolor_sink.h>

#ifdef __ANDROID__
#include <spdlog/sinks/android_sink.h>
#endif

spdlog::level::level_enum loglevel_string_to_enum(const char* value) {
    for (size_t level_number = 0; level_number < sizeof(spdlog::level::level_names)/sizeof(spdlog::level::level_names[0]); level_number++) {
        if (strcmp(value, spdlog::level::level_names[level_number]) == 0) {
            return static_cast<spdlog::level::level_enum>(level_number);
        }
    }
    logger::misc().critical("invalid loglevel name: %s", value);
    exit(109);
}

static std::vector<spdlog::sink_ptr> create_holded_sinks()
{
    std::vector<spdlog::sink_ptr> result;
//        result.push_back(std::make_shared<spdlog::sinks::stdout_sink_st>());
#ifdef __ANDROID__
    result.push_back(std::make_shared<spdlog::sinks::android_sink>("libtunnel"));
#else
    result.push_back(std::make_shared<spdlog::sinks::ansicolor_stdout_sink_st>());
#endif
    return result;
}

static std::vector<spdlog::sink_ptr>& get_holded_sinks() {
    static std::vector<spdlog::sink_ptr> holded_sinks = create_holded_sinks();
    return holded_sinks;
}

logger::logger(const char* name)
    : spdlog::logger(name, get_holded_sinks().begin(), get_holded_sinks().end())
{
    const char* maximum_working_loglevel_name = getenv("MAXIMUM_WORKING_LOGLEVEL");
    spdlog::level::level_enum maximum_working_loglevel = maximum_working_loglevel_name == NULL ? spdlog::level::debug : loglevel_string_to_enum(maximum_working_loglevel_name);

    set_level(maximum_working_loglevel);
}

void set_log_level(spdlog::level::level_enum log_level)
{
    for (spdlog::sink_ptr& sink: get_holded_sinks()) {
        sink->set_level(log_level);
    }
}
