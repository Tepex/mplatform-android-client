#include <sys/prctl.h>
#include <linux/capability.h>
#include <cap-ng.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>


void setup_ambient_capabilities()
{
    capng_get_caps_process();
    int rc = capng_update(CAPNG_ADD, CAPNG_INHERITABLE, CAP_NET_ADMIN);
    if (rc) {
        printf("Cannot add inheritable cap\n");
        exit(2);
    }
    capng_apply(CAPNG_SELECT_CAPS);

    if (prctl(PR_CAP_AMBIENT, PR_CAP_AMBIENT_RAISE, CAP_NET_ADMIN, 0, 0)) {
        perror("Cannot set cap");
        exit(1);
    }

}


void reap_zombies(int unused) {
    pid_t kidpid;
    int status;

    while ((waitpid(-1, &status, WNOHANG)) > 0)
    {
        //TODO status code
        //EINTR???
    }
}

void setup_zombie_reaper() {
    struct sigaction sa;

    sa.sa_handler = &reap_zombies;
    sa.sa_flags = 0;
    sigfillset(&sa.sa_mask);

    if (sigaction(SIGCHLD, &sa, NULL) == -1) {
        perror("Error: cannot setup SIGCHLD handler");
        exit(1);
    }
}
