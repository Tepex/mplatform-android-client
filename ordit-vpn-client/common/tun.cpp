#include "tun.h"
#include <sys/ioctl.h>
#include <net/if.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <linux/if_tun.h>
#include <unistd.h>
#include "system_linux.h"


int tun_alloc(const char *dev)
{

    struct ifreq ifr;
    int fd, err;
    const char *clonedev = "/dev/net/tun";

    if (dev == NULL) {
        fprintf(stderr, "dev should not be NULL\n");
        exit(1);
    }

    /* open the clone device */
    if ((fd = open(clonedev, O_RDWR)) < 0) {
        perror("Allocating interface");
        exit(1);
    }

    /* preparation of the struct ifr, of type "struct ifreq" */
    memset(&ifr, 0, sizeof(ifr));

    ifr.ifr_flags = IFF_TUN;    // for simpler compatibility with apple
    //ifr.ifr_flags =  IFF_TUN | IFF_NO_PI;

    if (*dev) {
        /* if a device name was specified, put it in the structure; otherwise,
         * the kernel will try to allocate the "next" device of the
         * specified type */
        strncpy(ifr.ifr_name, dev, IFNAMSIZ);
    }

    /* try to create the device */
    if ((err = ioctl(fd, TUNSETIFF, (void *)&ifr)) < 0) {
        close(fd);
        perror("Allocating interface");
        exit(1);
    }

    /* this is the special file descriptor that the caller will use to talk
     * with the virtual interface */

    return fd;
}

void configure_vpn_tunnel(int tun_fd, const std::string & interface_name, int association_id)
{
    const std::string command =
        std::string("./configure_vpn_tunnel ") + interface_name + std::string(" ") + std::to_string(association_id);
    int return_value = system(command.c_str());
    if (return_value == 127) {
        fprintf(stderr, "failed to execute shell\n");
        try_close(tun_fd);
        exit(1);
    }
    if (return_value < 0) {
        perror("error executing system()");
        try_close(tun_fd);
        exit(1);
    }
    if (return_value > 0) {
        fprintf(stderr, "vpn tunnel configuration error");
        try_close(tun_fd);
        exit(1);
    }
}
