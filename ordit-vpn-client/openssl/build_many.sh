#!/bin/bash

#git clone --branch OpenSSL_1_1_0f https://github.com/openssl/openssl source

#rm -rf ../prebuild/build-*
_ANDROID_ABI=x86 _ANDROID_API="android-16" CROSS_COMPILE=i686-linux-android- _ANDROID_EABI="x86-4.9" ARCH=x86 OPENSSL_TARGET=android-x86 ./build.sh
_ANDROID_ABI=armeabi-v7a _ANDROID_API="android-16" CROSS_COMPILE=arm-linux-androideabi- _ANDROID_EABI="arm-linux-androideabi-4.9" ARCH=arm OPENSSL_TARGET=android-armeabi ./build.sh
_ANDROID_ABI=arm64-v8a _ANDROID_API="android-21" CROSS_COMPILE=aarch64-linux-android- _ANDROID_EABI="aarch64-linux-android-4.9" ARCH=arm64 OPENSSL_TARGET=android64-aarch64 ./build.sh

cd source
make clean
./Configure linux-x86_64 shared no-ssl2 no-ssl3 no-comp no-hw no-engine --openssldir=`pwd`/../../prebuild/build-x86_64 --prefix=`pwd`/../../prebuild/build-x86_64 -Wa,--noexecstack
make depend
make
make install

#rm -rf source
