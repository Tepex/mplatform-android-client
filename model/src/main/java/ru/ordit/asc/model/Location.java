package ru.ordit.asc.model;

import java.util.ArrayList;
import java.util.List;

public class Location
{
    public Location(String name, String code)
    {
        this.name = name;
        this.code = code;
    }
    
    public String getName()
    {
        return name;
    }
    
    public String getCode()
    {
        return code;
    }
    
	@Override
	public String toString()
	{
	    return name+" "+code;
	}
	
	public static List<Location> getLocations()
	{
	    return locations;
	}
	
	public static Location getDefaultLocation()
	{
	    return locations.get(0);
	}
	
	private String name;
	private String code;
	
	private static final List<Location> locations = new ArrayList<>();
	
	static
	{
	    locations.add(new Location("United States", "🇺🇸"));
        locations.add(new Location("Vietnam", "🇻🇳"));
        locations.add(new Location("Georgia", "🇬🇪"));
        locations.add(new Location("Palau", "🇵🇼"));
        locations.add(new Location("United Kingdom", "🇬🇧"));
	    locations.add(new Location("Poland", "🇵🇱"));
	}
}
