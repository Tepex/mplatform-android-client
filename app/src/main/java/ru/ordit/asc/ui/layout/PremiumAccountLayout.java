package ru.ordit.asc.ui.layout;

import android.content.Context;

import android.graphics.Canvas;
import android.graphics.Color;

import android.util.AttributeSet;

public class PremiumAccountLayout extends BaseLayout
{
	public PremiumAccountLayout(Context context)
	{
		super(context);
	}
	
	public PremiumAccountLayout(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}
	
	public PremiumAccountLayout(Context context, AttributeSet attrs, int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);
	}
	
	@Override
	public void onDraw(Canvas canvas)
	{
		super.onDraw(canvas);
		drawStar(canvas, 320f/360, 100f/640, STAR_SMALL, PINK);
		drawStar(canvas, 130f/360, 157f/640, STAR_MEDIUM, BLUE);
		drawStar(canvas, 30f/360, 190f/640, STAR_MEDIUM, PINK);
		drawStar(canvas, 60f/360, 270f/640, STAR_SMALL, YELLOW);
		drawStar(canvas, 240f/360, 280f/640, STAR_BIG, YELLOW);
		drawStar(canvas, 320f/360, 370f/640, STAR_MEDIUM, Color.BLACK);
	}
}
