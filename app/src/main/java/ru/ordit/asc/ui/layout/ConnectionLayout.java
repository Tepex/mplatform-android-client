package ru.ordit.asc.ui.layout;

import android.content.Context;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import android.support.annotation.DrawableRes;

import android.util.AttributeSet;

import ru.ordit.asc.R;

public class ConnectionLayout extends BaseLayout
{
	public ConnectionLayout(Context context)
	{
		super(context);
	}
	
	public ConnectionLayout(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}
	
	public ConnectionLayout(Context context, AttributeSet attrs, int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);
	}
	
	@Override
	public void onDraw(Canvas canvas)
	{
		super.onDraw(canvas);
		if(backgroundId == R.drawable.bgr_belgium) drawBelgium(canvas);
		else if(backgroundId == R.drawable.bgr_czech) drawCzech(canvas);
		else if(backgroundId == R.drawable.bgr_finland) drawFinland(canvas);
		else if(backgroundId == R.drawable.bgr_france) drawFrance(canvas);
		else if(backgroundId == R.drawable.bgr_germany) drawGermany(canvas);
		else if(backgroundId == R.drawable.bgr_ireland) drawIreland(canvas);
		else if(backgroundId == R.drawable.bgr_italy) drawItaly(canvas);
		else if(backgroundId == R.drawable.bgr_lithuania) drawLithuania(canvas);
		else if(backgroundId == R.drawable.bgr_netherlands) drawNetherlands(canvas);
		else if(backgroundId == R.drawable.bgr_poland) drawPoland(canvas);
		else if(backgroundId == R.drawable.bgr_portugal) drawPortugal(canvas);
		else if(backgroundId == R.drawable.bgr_spain) drawSpain(canvas);
		else if(backgroundId == R.drawable.bgr_uk) drawUk(canvas);
		else if(backgroundId == R.drawable.bgr_usa) drawUsa(canvas);
		else
		{
			drawStar(canvas, 20f/360, 83f/640, STAR_BIG, Color.BLACK);
			drawStar(canvas, 266f/360, 225f/640, STAR_MEDIUM, Color.BLACK);
		
			drawStar(canvas, 320f/360, 187f/640, STAR_MEDIUM, BLUE);
			drawStar(canvas, 20f/360, 448f/640, STAR_MEDIUM, BLUE);
			drawStar(canvas, 263f/360, 360f/640, STAR_SMALL, BLUE);
		
			drawStar(canvas, 287f/360, 128f/640, STAR_SMALL, YELLOW);
		}
	}
	
	public void setBackgroundId(@DrawableRes int backgroundId)
	{
		this.backgroundId = backgroundId;
		setBackgroundResource(backgroundId);
	}
	
	private void drawBelgium(Canvas canvas)
	{
		drawStar(canvas, 320f/360, 60f/640, STAR_SMALL, YELLOW);
		drawStar(canvas, 215f/360, 85f/640, STAR_SMALL, Color.WHITE);
		drawStar(canvas, 50f/360, 100f/640, STAR_MEDIUM, Color.BLACK);
		drawStar(canvas, 110f/360, 110f/640, STAR_SMALL, Color.BLACK);
		drawStar(canvas, 310f/360, 195f/640, STAR_BIG, BLUE);
		drawStar(canvas, 100f/360, 255f/640, STAR_SMALL, PINK);
		drawStar(canvas, 20f/360, 265f/640, STAR_SMALL, Color.WHITE);
	}
	
	private void drawCzech(Canvas canvas)
	{
		drawStar(canvas, 315f/360, 80f/640, STAR_SMALL, YELLOW);
		drawStar(canvas, 210f/360, 90f/640, STAR_MEDIUM, Color.WHITE);
		drawStar(canvas, 45f/360, 100f/640, STAR_BIG, Color.BLACK);
		drawStar(canvas, 160f/360, 170f/640, STAR_SMALL, Color.BLACK);
		drawStar(canvas, 240f/360, 180f/640, STAR_BIG, BLUE);
		drawStar(canvas, 80f/360, 210f/640, STAR_MEDIUM, PINK);
		drawStar(canvas, 12f/360, 270f/640, STAR_SMALL, Color.WHITE);
		
		float x = getWidth() * (192f/360);
		float y = getHeight() * (188f/640);
		Paint paint = new Paint();
		paint.setColor(Color.WHITE);
		paint.setAlpha((int)(0.267 * 255));
		canvas.drawCircle(x, y, dpToPx(14), paint);
		paint.setAlpha((int)(0.134 * 255));
		canvas.drawCircle(x, y, dpToPx(30), paint);
	}
	
	private void drawFinland(Canvas canvas)
	{
		drawStar(canvas, 315f/360, 45f/640, STAR_SMALL, Color.WHITE);
		drawStar(canvas, 210f/360, 55f/640, STAR_MEDIUM, Color.WHITE);
		drawStar(canvas, 45f/360, 65f/640, STAR_BIG, Color.WHITE);
		drawStar(canvas, 335f/360, 85f/640, STAR_SMALL, Color.WHITE);
		drawStar(canvas, 110f/360, 90f/640, STAR_MEDIUM, Color.WHITE);
		drawStar(canvas, 265f/360, 105f/640, STAR_SMALL, Color.WHITE);
		drawStar(canvas, 160f/360, 130f/640, STAR_MEDIUM, Color.WHITE);
		drawStar(canvas, 230f/360, 150f/640, STAR_BIG, Color.WHITE);
		drawStar(canvas, 60f/360, 175f/640, STAR_BIG, Color.WHITE);
		
		drawStar(canvas, 310f/360, 195f/640, STAR_MEDIUM, Color.WHITE);
		drawStar(canvas, 240f/360, 220f/640, STAR_MEDIUM, Color.WHITE);
		drawStar(canvas, 325f/360, 230f/640, STAR_MEDIUM, Color.WHITE);
		
		drawStar(canvas, 15f/360, 230f/640, STAR_SMALL, Color.WHITE);
		drawStar(canvas, 33f/360, 280f/640, STAR_SMALL, Color.WHITE);
	}
	
	private void drawUsa(Canvas canvas)
	{
		drawStar(canvas, 215f/360, 45f/640, STAR_MEDIUM, PINK);
		drawStar(canvas, 135f/360, 80f/640, STAR_MEDIUM, BLUE);
		drawStar(canvas, 315f/360, 82f/640, STAR_SMALL, YELLOW);
		drawStar(canvas, 185f/360, 120f/640, STAR_SMALL, YELLOW);
		drawStar(canvas, 45f/360, 95f/640, STAR_BIG, Color.BLACK);
		drawStar(canvas, 240f/360, 145f/640, STAR_MEDIUM, Color.BLACK);
		
		drawStar(canvas, 188f/360, 230f/640, STAR_MEDIUM, PINK);
		drawStar(canvas, 320f/360, 250f/640, STAR_MEDIUM, PINK);
		
		drawStar(canvas, 30f/360, 260f/640, STAR_SMALL, YELLOW);
		drawStar(canvas, 250f/360, 290f/640, STAR_BIG, YELLOW);
		
		float x = getWidth() * (72f/360);
		float y = getHeight() * (150f/640);
		Paint paint = new Paint();
		paint.setColor(Color.WHITE);
		paint.setAlpha(18);
		canvas.drawCircle(x, y, dpToPx(38), paint);
		paint.setAlpha(30);
		canvas.drawCircle(x, y, dpToPx(25), paint);
	}
	
	private void drawFrance(Canvas canvas)
	{
		drawStar(canvas, 215f/360, 45f/640, STAR_MEDIUM, PINK);
		drawStar(canvas, 110f/360, 175f/640, STAR_MEDIUM, PINK);
		drawStar(canvas, 320f/360, 250f/640, STAR_MEDIUM, PINK);
		
		drawStar(canvas, 315f/360, 80f/640, STAR_SMALL, YELLOW);
		drawStar(canvas, 180f/360, 120f/640, STAR_SMALL, YELLOW);
		drawStar(canvas, 30f/360, 260f/640, STAR_SMALL, YELLOW);
		
		drawStar(canvas, 135f/360, 80f/640, STAR_MEDIUM, BLUE);
		drawStar(canvas, 45f/360, 100f/640, STAR_BIG, Color.BLACK);
		drawStar(canvas, 240f/360, 145f/640, STAR_MEDIUM, Color.BLACK);
	}
	
	private void drawGermany(Canvas canvas)
	{
		drawStar(canvas, 215f/360, 45f/640, STAR_MEDIUM, PINK);
		drawStar(canvas, 320f/360, 250f/640, STAR_MEDIUM, PINK);
		
		drawStar(canvas, 45f/360, 100f/640, STAR_BIG, Color.BLACK);
		drawStar(canvas, 175f/360, 150f/640, STAR_MEDIUM, Color.BLACK);
		
		drawStar(canvas, 135f/360, 80f/640, STAR_MEDIUM, BLUE);
		drawStar(canvas, 315f/360, 80f/640, STAR_SMALL, YELLOW);
		drawStar(canvas, 30f/360, 260f/640, STAR_SMALL, YELLOW);
		drawStar(canvas, 250f/360, 290f/640, STAR_BIG, YELLOW);
	}
	
	private void drawIreland(Canvas canvas)
	{
		drawStar(canvas, 315f/360, 80f/640, STAR_SMALL, YELLOW);
		drawStar(canvas, 45f/360, 100f/640, STAR_BIG, Color.BLACK);
		drawStar(canvas, 160f/360, 170f/640, STAR_SMALL, Color.BLACK);
		
		drawStar(canvas, 210f/360, 90f/640, STAR_MEDIUM, Color.WHITE);
		drawStar(canvas, 10f/360, 270f/640, STAR_SMALL, Color.WHITE);
		
		drawStar(canvas, 230f/360, 190f/640, STAR_BIG, BLUE);
		drawStar(canvas, 80f/360, 210f/640, STAR_MEDIUM, PINK);
	}
	
	private void drawItaly(Canvas canvas)
	{
		drawStar(canvas, 215f/360, 45f/640, STAR_MEDIUM, PINK);
		drawStar(canvas, 320f/360, 250f/640, STAR_MEDIUM, PINK);
		
		drawStar(canvas, 315f/360, 80f/640, STAR_SMALL, YELLOW);
		drawStar(canvas, 30f/360, 260f/640, STAR_SMALL, YELLOW);
		drawStar(canvas, 250f/360, 290f/640, STAR_BIG, YELLOW);
		
		drawStar(canvas, 45f/360, 100f/640, STAR_BIG, Color.BLACK);
		drawStar(canvas, 155f/360, 150f/640, STAR_MEDIUM, Color.BLACK);
		drawStar(canvas, 135f/360, 80f/640, STAR_MEDIUM, BLUE);
	}
	
	private void drawLithuania(Canvas canvas)
	{
		drawStar(canvas, 50f/360, 100f/640, STAR_BIG, Color.BLACK);
		drawStar(canvas, 165f/360, 165f/640, STAR_SMALL, Color.BLACK);
		
		drawStar(canvas, 320f/360, 75f/640, STAR_SMALL, YELLOW);
		
		drawStar(canvas, 215f/360, 85f/640, STAR_MEDIUM, Color.WHITE);
		drawStar(canvas, 20f/360, 265f/640, STAR_SMALL, Color.WHITE);
		
		drawStar(canvas, 85f/360, 185f/640, STAR_MEDIUM, PINK);
		drawStar(canvas, 240f/360, 190f/640, STAR_BIG, BLUE);
	}
	
	private void drawNetherlands(Canvas canvas)
	{
		drawStar(canvas, 109.5f/360, 201f/640, STAR_MEDIUM, PINK);
	}
	
	private void drawPoland(Canvas canvas)
	{
		drawStar(canvas, 315f/360, 80f/640, STAR_SMALL, YELLOW);
		drawStar(canvas, 45f/360, 100f/640, STAR_BIG, Color.BLACK);
		drawStar(canvas, 145f/360, 110f/640, STAR_SMALL, Color.BLACK);
		drawStar(canvas, 290f/360, 160f/640, STAR_BIG, BLUE);
		
		drawStar(canvas, 10f/360, 265f/640, STAR_SMALL, Color.WHITE);
	}
	
	private void drawPortugal(Canvas canvas)
	{
		drawStar(canvas, 315f/360, 80f/640, STAR_SMALL, YELLOW);
		
		drawStar(canvas, 210f/360, 90f/640, STAR_MEDIUM, Color.WHITE);
		drawStar(canvas, 10f/360, 265f/640, STAR_SMALL, Color.WHITE);
		
		drawStar(canvas, 45f/360, 100f/640, STAR_BIG, Color.BLACK);
		drawStar(canvas, 160f/360, 170f/640, STAR_SMALL, Color.BLACK);
		
		drawStar(canvas, 80f/360, 210f/640, STAR_MEDIUM, PINK);
		drawStar(canvas, 240f/360, 180f/640, STAR_BIG, BLUE);
		
	}
	
	private void drawSpain(Canvas canvas)
	{
		drawStar(canvas, 215f/360, 45f/640, STAR_MEDIUM, PINK);
		drawStar(canvas, 260f/360, 170f/640, STAR_MEDIUM, PINK);
		drawStar(canvas, 315f/360, 80f/640, STAR_SMALL, YELLOW);
		drawStar(canvas, 30f/360, 260f/640, STAR_SMALL, YELLOW);
		drawStar(canvas, 285f/360, 300f/640, STAR_BIG, YELLOW);
		
		drawStar(canvas, 185f/360, 115f/640, STAR_MEDIUM, BLUE);
		
		drawStar(canvas, 45f/360, 100f/640, STAR_BIG, Color.BLACK);
		drawStar(canvas, 110f/360, 190f/640, STAR_MEDIUM, Color.BLACK);
		
		drawStar(canvas, 173f/360, 158f/640, STAR_MEDIUM, YELLOW);
		drawStar(canvas, 209f/360, 156f/640, STAR_MEDIUM, YELLOW);
		drawStar(canvas, 158f/360, 179f/640, STAR_MEDIUM, YELLOW);
		drawStar(canvas, 226f/360, 175f/640, STAR_MEDIUM, YELLOW);
		drawStar(canvas, 196f/360, 255f/640, STAR_MEDIUM, YELLOW);
	}
	
	private void drawUk(Canvas canvas)
	{
		drawStar(canvas, 315f/360, 80f/640, STAR_SMALL, YELLOW);
		drawStar(canvas, 45f/360, 100f/640, STAR_BIG, Color.BLACK);
		
		drawStar(canvas, 310f/360, 315f/640, STAR_BIG, BLUE);
		
		drawStar(canvas, 10f/360, 265f/640, STAR_SMALL, Color.WHITE);
		drawStar(canvas, 251f/360, 257f/640, STAR_SMALL, Color.WHITE);
	}
	
	@DrawableRes 
	private int backgroundId;
}
