package ru.ordit.asc.ui.layout;

import android.content.Context;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import android.support.constraint.ConstraintLayout;

import android.util.AttributeSet;
import android.util.DisplayMetrics;

public class BaseLayout extends ConstraintLayout
{
	public BaseLayout(Context context)
	{
		super(context);
		init();
	}
	
	public BaseLayout(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		init();
	}
	
	public BaseLayout(Context context, AttributeSet attrs, int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);
		init();
	}
	
	protected void init()
	{
		metrics = getResources().getDisplayMetrics();
	}

	/**
	 * @param xp x координата в долях от ширины.
	 * @param yp y координата в долях от высоты.
	 */
	protected void drawStar(Canvas canvas, float xp, float yp, float radius, int color)
	{
		Paint paint = new Paint();
		paint.setStyle(Paint.Style.FILL);
		paint.setColor(color);
		
		float x = getWidth() * xp;
		float y = getHeight() * yp;
		canvas.drawCircle(x, y, dpToPx(radius), paint);
	}
		/*
		
canvas.translate(getWidth()/2f,getHeight()/2f);
canvas.drawCircle(0,0, radius, paint);		
		*/
	
	public float dpToPx(float dp)
	{
		return dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
	}
	
	public float pxToDp(float px)
	{
		return px / ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
	}
	
	public static final int YELLOW = 0xffffe02c;
	public static final int PINK = 0xffff76b8;
	public static final int BLUE = 0xff6275ff;
	
	public static final float STAR_BIG = 8f;
	public static final float STAR_MEDIUM = 6f;
	public static final float STAR_SMALL = 4f;
	
	private DisplayMetrics metrics;
}
