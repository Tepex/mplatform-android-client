package ru.ordit.asc.ui;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ru.ordit.asc.R;

public abstract class BaseActivity extends AppCompatActivity
{
	@Override
	@CallSuper
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(getLayoutId());
		unbinder = ButterKnife.bind(this);
		prefs = getSharedPreferences("ru.ordit.asc.prefs", MODE_PRIVATE);
	}
	
	@CallSuper
	@Override
	protected void onDestroy()
	{
		if(unbinder != null) unbinder.unbind();
		unbinder = null;
		super.onDestroy();
	}
	
	protected void startActivity(Class activityClass, Bundle extras, boolean isTop)
	{
		Intent intent = new Intent(this, activityClass);
		if(extras != null) intent.putExtras(extras);
		if(isTop) intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
		if(isTop) finish();
	}

    protected Dialog showAlertDialog(@StringRes int titleId, @StringRes int msgId)
	{
	    final Dialog dialog = new Dialog(this);
	    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	    dialog.setCancelable(true);
	    dialog.setCanceledOnTouchOutside(true);
	    dialog.setContentView(R.layout.custom_dialog);
	    ((TextView)dialog.findViewById(R.id.tv_title)).setText(titleId);
	    ((TextView)dialog.findViewById(R.id.tv_content)).setText(msgId);
        dialog.findViewById(R.id.btn_ok).setOnClickListener((v) -> dialog.dismiss());
        return dialog;
	}

	abstract @LayoutRes int getLayoutId();
	
	@SuppressWarnings("WeakerAccess")
    Unbinder unbinder;
    
    protected SharedPreferences prefs;
}
