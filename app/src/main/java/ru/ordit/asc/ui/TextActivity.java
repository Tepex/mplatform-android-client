package ru.ordit.asc.ui;

import android.os.Bundle;

import android.support.annotation.CallSuper;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.StringRes;

import android.support.v7.widget.Toolbar;

import butterknife.BindView;

import com.orhanobut.logger.Logger;

import ru.ordit.asc.R;

public class TextActivity extends BaseActivity
{
	@Override
	@CallSuper
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		
		/* установка title прежде, чем setSupportActionBar(toolbar) */
		toolbar.setTitle(getIntent().getExtras().getString(TITLE));
		
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayShowTitleEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
	}
	
	@Override
	protected @LayoutRes int getLayoutId()
	{
		return getIntent().getExtras().getInt(LAYOUT_ID);
	}
	
	/* !!! https://stackoverflow.com/a/26656285 */
	@Override
	public boolean onSupportNavigateUp()
	{
	    onBackPressed();
	    return true;
	}
	
	public static final String LAYOUT_ID = "layout_id";
	public static final String TITLE = "title";
	
	@SuppressWarnings("WeakerAccess")
	@BindView(R.id.toolbar) Toolbar toolbar;
}
