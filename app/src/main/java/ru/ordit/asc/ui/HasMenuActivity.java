package ru.ordit.asc.ui;

import android.content.ActivityNotFoundException;
import android.content.Intent;

import android.net.Uri;

import android.os.Bundle;

import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;

import com.orhanobut.logger.Logger;

import ru.ordit.asc.App;
import ru.ordit.asc.R;

public abstract class HasMenuActivity extends BaseActivity
{
	@Override
	@CallSuper
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		iconMenu.setOnClickListener(v ->
			startActivityForResult(new Intent(HasMenuActivity.this, MenuActivity.class), 
								   MenuActivity.REQUEST_CODE));
	}
	
	@Override
	@CallSuper
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if(requestCode == MenuActivity.REQUEST_CODE)
		{
			int menuIndex = data.getIntExtra(MenuActivity.ITEM_ID, MenuActivity.ITEM_NOTHING);
			switch(menuIndex)
			{
				case MENU_ACCOUNT:
					startAccount();
					break;
				case MENU_NEED_HELP:
					startNeedHelp();
					break;
				case MENU_TELL_A_FRIEND:
					startShareFriend();
					break;
				case MENU_PRIVACY_POLICY:
					startTextActivity(R.layout.activity_privacy, menuIndex);
					break;
				case MENU_ABOUT:
					startTextActivity(R.layout.activity_about, menuIndex);
					break;
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	private void startAccount()
	{
		if(App.getApp().getPremium()) startActivity(PremiumAccountActivity.class, null, false);
		else startActivity(TryPremiumMenuActivity.class, null, false);
	}
	
	private void startNeedHelp()
	{
		Intent intent = new Intent(Intent.ACTION_SENDTO);
		intent.setData(Uri.parse("mailto:"+getString(R.string.support_email)));
		intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.support_subject));
		try
		{
			startActivity(Intent.createChooser(intent, getString(R.string.support_select_app)));
		}
		catch(ActivityNotFoundException ex)
		{
			Toast.makeText(this, "No email clients installed.", Toast.LENGTH_LONG).show();
		}
	}
	
	private void startShareFriend()
	{
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.setType("text/plain");
		intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.share_text));
		startActivity(intent);
	}
	
	private void startTextActivity(@LayoutRes int layoutId, int menuIndex)
	{
		Bundle bundle = new Bundle();
		bundle.putInt(TextActivity.LAYOUT_ID, layoutId);
		bundle.putString(TextActivity.TITLE, getResources().getStringArray(R.array.menu)[menuIndex]);
		startActivity(TextActivity.class, bundle, false);
	}
	
	@BindView(R.id.hamburger) ImageView iconMenu;
	
	public static final int MENU_ACCOUNT = 0;
	public static final int MENU_NEED_HELP = 1;
	public static final int MENU_TELL_A_FRIEND = 2;
	
	public static final int MENU_PRIVACY_POLICY = 5;
	public static final int MENU_ABOUT = 6;
}
