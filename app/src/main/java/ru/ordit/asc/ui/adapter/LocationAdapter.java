package ru.ordit.asc.ui.adapter;

import android.support.v7.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.orhanobut.logger.Logger;

import java.util.List;

import ru.ordit.asc.R;
import ru.ordit.asc.ui.interactor.LocationInteractor;
import ru.ordit.asc.ui.model.Location;

/**
https://habrahabr.ru/post/258195/
*/
public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.ViewHolder>
{
	public LocationAdapter(int currentLocation, OnClickListener callback)
	{
		this.currentLocation = currentLocation;
		this.callback = callback;
	}
	
	@Override
	public LocationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
	{
		View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
		ViewHolder vh = new ViewHolder(v, callback);
		return vh;
	}
	
	@Override
	public void onBindViewHolder(ViewHolder holder, int pos)
	{
		Location location = locationInteractor.getLocation(pos);
		holder.imgCountry.setImageResource(location.getImageId());
		holder.tvCountryName.setText(location.getNameId());
        holder.imgCheckbox.setVisibility((pos == currentLocation) ? View.VISIBLE : View.GONE);
    }
    
    @Override
    public int getItemCount()
    {
        return locationInteractor.getLocationSize();
    }
    
    private int currentLocation;
    private OnClickListener callback;
    private LocationInteractor locationInteractor = LocationInteractor.getInstance();
    
    public static interface OnClickListener
    {
        public void onClick(int loc);
    }
    
	public static class ViewHolder extends RecyclerView.ViewHolder
	{
		public ViewHolder(View view, final OnClickListener callback)
		{
			super(view);
			ButterKnife.bind(this, view);
			view.setOnClickListener(v -> callback.onClick(getAdapterPosition()));
        }
        
        @BindView(R.id.country_code) ImageView imgCountry;
        @BindView(R.id.country_name) TextView tvCountryName;
        @BindView(R.id.country_checkbox) ImageView imgCheckbox;
    }
}
