package ru.ordit.asc.ui.model;

import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;

public class Location
{
	public Location(@StringRes int nameId, @DrawableRes int imgId, @DrawableRes int backgroundId)
	{
		this.nameId = nameId;
		this.imgId = imgId;
		this.backgroundId = backgroundId;
	}
	
	public @StringRes int getNameId()
	{
		return nameId;
	}
	
	public @DrawableRes int getImageId()
	{
		return imgId;
	}
	
	public @DrawableRes int getBackgroundId()
	{
		return backgroundId;
	}
	
	@Override
	public int hashCode()
	{
		return imgId;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj == null || !(obj instanceof Location)) return false;
		Location other = (Location)obj;
		return (other.imgId == imgId && other.nameId == nameId);
	}
	
	private @StringRes int nameId;
	private @DrawableRes int imgId;
	private @DrawableRes int backgroundId;
}
