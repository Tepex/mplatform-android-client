package ru.ordit.asc.ui;

import android.content.Intent;

import android.os.Bundle;

import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;

import android.support.v7.widget.Toolbar;

import android.view.View;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import butterknife.BindView;

import com.orhanobut.logger.Logger;

import ru.ordit.asc.App;
import ru.ordit.asc.R;

public class TryPremiumActivity extends BaseActivity
{
	@Override
	@CallSuper
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		
		toolbar.setTitle(R.string.toolbar_try_premium);
		
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayShowTitleEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);

		btnFree.setOnClickListener(v -> 
		{
			Toast.makeText(TryPremiumActivity.this, "7 days for free!", Toast.LENGTH_LONG).show();
			startPremium();
		});
		btnMonth.setOnClickListener(v -> 
		{
			Toast.makeText(TryPremiumActivity.this, "Premium account!", Toast.LENGTH_LONG).show();
			startPremium();
		});
	}
	
	@Override
	public boolean onSupportNavigateUp()
	{
	    onBackPressed();
	    return true;
	}
	
	@Override
	protected @LayoutRes int getLayoutId()
	{
		return R.layout.activity_try_premium;
	}
	
	protected void startPremium()
	{
		App.getApp().setPremium(true);
		startActivity(ConnectionActivity.class, null, true);
	}
	
	@SuppressWarnings("WeakerAccess")
	@BindView(R.id.toolbar) Toolbar toolbar;
	@BindView(R.id.btn_free) View btnFree;
	@BindView(R.id.btn_month) Button btnMonth;
}
