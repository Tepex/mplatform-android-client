package ru.ordit.asc.ui.layout;

import android.content.Context;

import android.graphics.Canvas;
import android.graphics.Color;

import android.util.AttributeSet;

public class TryPremiumLayout extends BaseLayout
{
	public TryPremiumLayout(Context context)
	{
		super(context);
	}
	
	public TryPremiumLayout(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}
	
	public TryPremiumLayout(Context context, AttributeSet attrs, int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);
	}
	
	@Override
	public void onDraw(Canvas canvas)
	{
		super.onDraw(canvas);
		drawStar(canvas, 20f/360, 83f/640, STAR_BIG, Color.BLACK);
		drawStar(canvas, 266f/360, 225f/640, STAR_MEDIUM, Color.BLACK);
		
		drawStar(canvas, 320f/360, 187f/640, STAR_MEDIUM, BLUE);
		drawStar(canvas, 20f/360, 448f/640, STAR_MEDIUM, BLUE);
		drawStar(canvas, 263f/360, 360f/640, STAR_SMALL, BLUE);
		
		drawStar(canvas, 287f/360, 128f/640, STAR_SMALL, YELLOW);
	}
}
