package ru.ordit.asc.ui.adapter;

import android.support.v7.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.orhanobut.logger.Logger;

import ru.ordit.asc.R;

/**
https://habrahabr.ru/post/258195/
*/
public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.ViewHolder>
{
	public MenuAdapter(String[] data, OnClickListener callback)
	{
		this.data = data;
		this.callback = callback;
	}
	
	@Override
	public MenuAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
	{
		View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.menu_item, parent, false);
		ViewHolder vh = new ViewHolder(v, callback);
		return vh;
	}
	
	@Override
	public void onBindViewHolder(ViewHolder holder, int pos)
	{
		holder.tvItemName.setText(data[pos]);
	}
	
	@Override
	public int getItemCount()
	{
		return data.length;
	}
	
	private String[] data;
	private OnClickListener callback;
	
	public static interface OnClickListener
	{
		public void onClick(int pos);
	}
	
	public static class ViewHolder extends RecyclerView.ViewHolder
	{
		public ViewHolder(View view, OnClickListener callback)
		{
			super(view);
			ButterKnife.bind(this, view);
			view.setOnClickListener(v -> callback.onClick(getAdapterPosition()));
		}
		
		@BindView(R.id.item_name) TextView tvItemName;
	}
}
