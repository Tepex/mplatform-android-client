package ru.ordit.asc.ui;

import android.os.Bundle;

import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;

import android.view.View;

import android.widget.Button;

import butterknife.BindView;

import com.orhanobut.logger.Logger;

import ru.ordit.asc.R;

public class AdsActivity extends ConnectionActivity
{
	@Override
	@CallSuper
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		btnCloseAd.setOnClickListener(v -> changeLocation(v));
	}
	
	@Override
	protected @LayoutRes int getLayoutId()
	{
		return R.layout.activity_ads;
	}
	
	@Override
	public void changeLocation(View view)
	{
		startActivity(TryPremiumActivity.class, null, false);
	}
	
	@BindView(R.id.btn_close_ad) Button btnCloseAd;
}
