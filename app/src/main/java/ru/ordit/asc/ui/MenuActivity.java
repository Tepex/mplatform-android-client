package ru.ordit.asc.ui;

import android.content.Intent;

import android.os.Bundle;

import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.util.SparseIntArray;

import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;

import com.orhanobut.logger.Logger;

import ru.ordit.asc.R;
import ru.ordit.asc.ui.adapter.MenuAdapter;

public class MenuActivity extends BaseActivity
{
	@Override
	@CallSuper
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		iconClose.setOnClickListener(v->onBackPressed());
		iconA.setOnClickListener(v->Logger.i("A pressed"));
		
		recyclerView.setHasFixedSize(true);
		recyclerView.setLayoutManager(new LinearLayoutManager(this));
		recyclerView.setAdapter(new MenuAdapter(getResources().getStringArray(R.array.menu),
			                                    pos -> 
		{
			selection = pos;
			onBackPressed();
		}));
	}
	
	@Override
	protected @LayoutRes int getLayoutId()
	{
		return R.layout.activity_menu;
	}
	
	@Override
	@CallSuper
	public void onBackPressed()
	{
		Intent intent = new Intent();
		intent.putExtra(ITEM_ID, selection);
	    setResult(RESULT_OK, intent);
	    super.onBackPressed();
	}
	
	public static final int REQUEST_CODE = 300;
	public static final int ITEM_NOTHING = -1;
	public static final String ITEM_ID = "item_id";
	
	@BindView(R.id.close) ImageView iconClose;
	@BindView(R.id.A) ImageView iconA;
	@BindView(R.id.recycler_view) RecyclerView recyclerView;
	
	private int selection = ITEM_NOTHING;
}
