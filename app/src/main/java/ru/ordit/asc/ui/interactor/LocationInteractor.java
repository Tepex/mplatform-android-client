package ru.ordit.asc.ui.interactor;

import ru.ordit.asc.R;

import ru.ordit.asc.ui.model.Location;

public class LocationInteractor
{
	public static LocationInteractor getInstance()
	{
		return INSTANCE;
	}
	
	private LocationInteractor()
	{
	}
	
	public Location getLocation(int pos)
	{
		return locations[pos];
	}
	
	public int getLocationSize()
	{
		return locations.length;
	}
	
	private static LocationInteractor INSTANCE = new LocationInteractor();
	
	private static final Location[] locations = {
		new Location(R.string.country_belgium, R.drawable.belgium, R.drawable.bgr_belgium),
		new Location(R.string.country_czech_republic, R.drawable.czech_republic, R.drawable.bgr_czech),
		new Location(R.string.country_finland, R.drawable.finland, R.drawable.bgr_finland),
		new Location(R.string.country_france, R.drawable.france, R.drawable.bgr_france),
		new Location(R.string.country_germany, R.drawable.germany, R.drawable.bgr_germany),
		new Location(R.string.country_ireland, R.drawable.ireland, R.drawable.bgr_ireland),
		new Location(R.string.country_italy, R.drawable.italy, R.drawable.bgr_italy),
		new Location(R.string.country_lithuania, R.drawable.lithuania, R.drawable.bgr_lithuania),
		new Location(R.string.country_netherlands, R.drawable.netherlands, R.drawable.bgr_netherlands),
		new Location(R.string.country_poland, R.drawable.poland, R.drawable.bgr_poland),
		new Location(R.string.country_portugal, R.drawable.portugal, R.drawable.bgr_portugal),
		new Location(R.string.country_spain, R.drawable.spain, R.drawable.bgr_spain),
		new Location(R.string.country_uk, R.drawable.uk, R.drawable.bgr_uk),
		new Location(R.string.country_usa, R.drawable.usa, R.drawable.bgr_usa)
	};
}