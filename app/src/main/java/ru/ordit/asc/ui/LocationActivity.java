package ru.ordit.asc.ui;

import android.content.Intent;

import android.os.Bundle;

import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import butterknife.BindView;

import com.orhanobut.logger.Logger;

import ru.ordit.asc.R;
import ru.ordit.asc.ui.adapter.LocationAdapter;

public class LocationActivity extends BaseActivity
{
	@Override
	@CallSuper
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		
		/* установка title прежде, чем setSupportActionBar(toolbar) */
		toolbar.setTitle(R.string.title_location);
		
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayShowTitleEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		
		currentLocation = getIntent().getIntExtra(CURRENT_LOCATION, -1);
		
		recyclerView.setHasFixedSize(true);
		recyclerView.setLayoutManager(new LinearLayoutManager(this));
		recyclerView.setAdapter(new LocationAdapter(currentLocation, selected ->
		{
		    currentLocation = selected;
		    onBackPressed();
		}));
	}
	
	@Override
	protected @LayoutRes int getLayoutId()
	{
		return R.layout.activity_location;
	}
	
	/* !!! https://stackoverflow.com/a/26656285 */
	@Override
	public boolean onSupportNavigateUp()
	{
	    onBackPressed();
	    return true;
	}
	
	@Override
	@CallSuper
	public void onBackPressed()
	{
	    Logger.i("Current location: %d", currentLocation);
	    Intent intent = new Intent();
	    intent.putExtra(CURRENT_LOCATION, currentLocation);
	    setResult(RESULT_OK, intent);
	    super.onBackPressed();
	}
	
	@SuppressWarnings("WeakerAccess")
	@BindView(R.id.toolbar) Toolbar toolbar;
	@BindView(R.id.recycler_view) RecyclerView recyclerView;
	
	private int currentLocation;
	
	public static final int REQUEST_CODE = 200;
	public static final String CURRENT_LOCATION = "location";
}
