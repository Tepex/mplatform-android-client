package ru.ordit.asc.ui.layout;

import android.content.Context;

import android.graphics.Canvas;
import android.graphics.Color;

import android.util.AttributeSet;

public class FreeStartLayout extends BaseLayout
{
	public FreeStartLayout(Context context)
	{
		super(context);
	}
	
	public FreeStartLayout(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}
	
	public FreeStartLayout(Context context, AttributeSet attrs, int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);
	}

	@Override
	public void onDraw(Canvas canvas)
	{
		drawStar(canvas, 40f/360, 98f/640, STAR_BIG, Color.BLACK);
		drawStar(canvas, 300f/360, 220f/640, STAR_MEDIUM, Color.BLACK);
		
		drawStar(canvas, 28f/360, 256f/640, STAR_SMALL, YELLOW);
		drawStar(canvas, 180f/360, 118f/640, STAR_SMALL, YELLOW);
		drawStar(canvas, 312f/360, 79f/640, STAR_SMALL, YELLOW);
		drawStar(canvas, 244f/360, 287f/640, STAR_BIG, YELLOW);
		
		drawStar(canvas, 209f/360, 41f/640, STAR_MEDIUM, PINK);
		drawStar(canvas, 254f/360, 138f/640, STAR_MEDIUM, PINK);
		drawStar(canvas, 106f/360, 148f/640, STAR_MEDIUM, PINK);
		
		drawStar(canvas, 130f/360, 74f/640, STAR_MEDIUM, BLUE);
		/*
		
canvas.translate(getWidth()/2f,getHeight()/2f);
canvas.drawCircle(0,0, radius, paint);		
		*/
	}
}
