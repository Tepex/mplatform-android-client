package ru.ordit.asc.ui;

import android.app.Activity;

import android.graphics.drawable.Drawable;

import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;

import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;

import android.support.v4.content.ContextCompat;

import android.view.View;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;

import com.alohamobile.vpnclient.VpnConsumer;
import com.alohamobile.vpnclient.VpnLogService;
import com.alohamobile.vpnclient.VpnProvider;

import com.ordit.vpn.OrditVpnProvider;
import com.ordit.vpn.configuration.OrditConfigurationManager;
import com.ordit.vpn.transport.firebase.FirebaseTransport;

import com.orhanobut.logger.Logger;

import ru.ordit.asc.R;
import ru.ordit.asc.ui.interactor.LocationInteractor;
import ru.ordit.asc.ui.layout.ConnectionLayout;
import ru.ordit.asc.ui.model.Location;

public class ConnectionActivity extends HasMenuActivity implements VpnConsumer, VpnLogService
{
	@Override
	@CallSuper
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		
		connectDrawable = ContextCompat.getDrawable(this, R.drawable.btn_green);
		disconnectDrawable = ContextCompat.getDrawable(this, R.drawable.btn_pink);
		connectText = getString(R.string.connection_btn_connect);
		disconnectText = getString(R.string.connection_btn_disconnect);
		
		btnConnect.setOnClickListener(v ->
		{
			// TODO поставить ProgressBar
			btnConnect.setEnabled(false);
			if(connected) provider.stop();
			else provider.start();
		});
		
		if(savedInstanceState != null) connected = savedInstanceState.getBoolean(STATE_CONNECTED);
		
		OrditConfigurationManager manager = new OrditConfigurationManager(
			new FirebaseTransport(getApplicationContext()));
		SharedPreferences preferences = getSharedPreferences("vpn_preferences", Activity.MODE_PRIVATE);
		provider = new OrditVpnProvider(this, preferences, manager, this,
                showAlertDialog(R.string.title_dialog, R.string.dialog_content));
		updateConnectButton();
		updateLocation(prefs.getInt(STATE_LOCATION, 0));
	}
	
	@Override
	protected @LayoutRes int getLayoutId()
	{
		return R.layout.activity_premium;
	}
	
	@Override
	@CallSuper
	protected void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
	    outState.putBoolean(STATE_CONNECTED, connected);
	}

	@OnClick({R.id.location_name, R.id.location_code, R.id.location_change})
	public void changeLocation(View view)
	{
		Intent intent = new Intent(this, LocationActivity.class);
		intent.putExtra(LocationActivity.CURRENT_LOCATION, prefs.getInt(STATE_LOCATION, 0));
		startActivityForResult(intent, LocationActivity.REQUEST_CODE);
	}
	
	@Override
	@CallSuper
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if(requestCode == LocationActivity.REQUEST_CODE)
		{
			int loc = data.getIntExtra(LocationActivity.CURRENT_LOCATION, -1);
			if(loc != -1)
			{
				prefs.edit().putInt(STATE_LOCATION, loc).apply();
				updateLocation(loc);
			}
		}
		provider.onActivityResult(requestCode, resultCode);
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	@Override
	public Activity getContext()
	{
		return this;
	}
	
	@Override
	public String getApplicationId()
	{
		return null;
	}
	
	@Override
	public void onOpenVpnDisconnected()
	{
		connected = false;
		updateConnectButton();
	}
	
	@Override
	public void startVpnConnection() {}
	
	@Override
	public void onOpenVpnConnected()
	{
		connected = true;
		updateConnectButton();
	}
	
	@Override
	public void onVpnError()
	{
		connected = false;
		updateConnectButton();
	}
	
	@Override
	public void logError(@Nullable String s, @Nullable String s1, boolean b, boolean b1)
	{
	}
	
	@UiThread
	private void updateConnectButton()
	{
		// может прилететь после закрытия активити.
		if(btnConnect == null) return;
		
		if(connected)
		{
			btnConnect.setBackground(disconnectDrawable);
			btnConnect.setText(disconnectText);
		}
		else
		{
			btnConnect.setBackground(connectDrawable);
			btnConnect.setText(connectText);
		}
		btnConnect.setEnabled(true);
	}
	
	private void updateLocation(int loc)
	{
		Location location = locationInteractor.getLocation(loc);
		tvLocationName.setText(location.getNameId());
		imgLocation.setImageResource(location.getImageId());
		rootLayout.setBackgroundId(location.getBackgroundId());
	}

	@BindView(R.id.root) ConnectionLayout rootLayout;
	@BindView(R.id.location_name) TextView tvLocationName;
	@BindView(R.id.location_code) ImageView imgLocation;
	
	@BindView(R.id.location_change) TextView tvLocationChange;
	@BindView(R.id.btn_connect) Button btnConnect;
	
	private VpnProvider provider;
	private boolean connected;
	private boolean showDialog = true;
	private LocationInteractor locationInteractor = LocationInteractor.getInstance();
	
	private Drawable connectDrawable;
	private Drawable disconnectDrawable;
	private String connectText;
	private String disconnectText;
	
	public static final String STATE_CONNECTED = "connected";
	public static final String STATE_LOCATION = "location";
}
