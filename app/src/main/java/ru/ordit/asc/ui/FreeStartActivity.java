package ru.ordit.asc.ui;

import android.content.Intent;

import android.os.Bundle;

import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;

import com.orhanobut.logger.Logger;

import ru.ordit.asc.R;

public class FreeStartActivity extends HasMenuActivity
{
	@Override
	@CallSuper
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		btnPremium.setOnClickListener(v -> startActivity(TryPremiumActivity.class, null, false));
		tvAds.setOnClickListener(v -> startActivity(AdsActivity.class, null, false));
	}
	
	@Override
	protected @LayoutRes int getLayoutId()
	{
		return R.layout.activity_free_start;
	}
	
	@BindView(R.id.premium)	Button btnPremium;
	@BindView(R.id.ads)	TextView tvAds;
}
