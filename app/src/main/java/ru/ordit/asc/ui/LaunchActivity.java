package ru.ordit.asc.ui;

import android.os.Bundle;

import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;

import android.support.v7.app.AppCompatDelegate;

import android.widget.Button;

import butterknife.BindView;

import ru.ordit.asc.App;
import ru.ordit.asc.R;

public class LaunchActivity extends BaseActivity
{
	@Override
	@CallSuper
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		btnWelcome.setOnClickListener(v ->
		{
			Boolean isPremium = App.getApp().getPremium();
			if(isPremium == null)
			{
				App.getApp().setPremium(false);
				startActivity(FreeStartActivity.class, null, true);
			}
			else if(!isPremium) startActivity(AdsActivity.class, null, true);
			else startActivity(ConnectionActivity.class, null, true);
		});
	}
	
	@Override
	protected @LayoutRes int getLayoutId()
	{
		return R.layout.activity_launch;
	}
	
	@BindView(R.id.btn_welcome) Button btnWelcome;
	
	// Черная магия!
	// https://stackoverflow.com/questions/35622438/update-android-support-library-to-23-2-0-cause-error-xmlpullparserexception-bin
	static
	{
		AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
	}
}
