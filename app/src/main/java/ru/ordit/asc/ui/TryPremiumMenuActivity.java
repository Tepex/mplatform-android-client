package ru.ordit.asc.ui;

import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;

import com.orhanobut.logger.Logger;

import ru.ordit.asc.R;

public class TryPremiumMenuActivity extends TryPremiumActivity
{
	@Override
	protected @LayoutRes int getLayoutId()
	{
		return R.layout.activity_try_premium_menu;
	}
}
