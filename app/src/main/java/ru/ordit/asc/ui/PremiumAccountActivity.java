package ru.ordit.asc.ui;

import android.content.Intent;

import android.os.Bundle;

import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;

import android.support.v7.widget.Toolbar;

import android.view.View;

import android.widget.Toast;

import butterknife.BindView;

import com.orhanobut.logger.Logger;

import ru.ordit.asc.App;
import ru.ordit.asc.R;

public class PremiumAccountActivity extends BaseActivity
{
	@Override
	@CallSuper
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		
		toolbar.setTitle(R.string.toolbar_premium_account);
		
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayShowTitleEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		
		btnCancel.setOnClickListener(v -> 
		{
			App.getApp().setPremium(false);
			startActivity(ConnectionActivity.class, null, true);
		});
		
	}
	
	@Override
	public boolean onSupportNavigateUp()
	{
	    onBackPressed();
	    return true;
	}
	
	@Override
	protected @LayoutRes int getLayoutId()
	{
		return R.layout.activity_premium_account;
	}
	
	@SuppressWarnings("WeakerAccess")
	@BindView(R.id.toolbar) Toolbar toolbar;
	@BindView(R.id.btn_cancel) View btnCancel;
}
