package ru.ordit.asc;

import android.app.Application;

import android.content.Context;
import android.content.SharedPreferences;

import android.support.annotation.CallSuper;

import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.FormatStrategy;
import com.orhanobut.logger.Logger;
import com.orhanobut.logger.PrettyFormatStrategy;

import com.squareup.leakcanary.LeakCanary;

public class App extends Application
{
	public static App getApp()
	{
		return app;
	}
	
	@Override
	@CallSuper
	public void onCreate()
	{
		super.onCreate();
		app = this;
		prefs = getSharedPreferences("ru.ordit.asc.prefs", MODE_PRIVATE);
		if(prefs.contains(PREFS_PREMIUM)) premium = prefs.getBoolean(PREFS_PREMIUM, false);
		// This process is dedicated to LeakCanary for heap analysis.
		// You should not init your app in this process.
		if(LeakCanary.isInAnalyzerProcess(this)) return;
		LeakCanary.install(this);
		
		FormatStrategy formatStrategy = PrettyFormatStrategy.newBuilder()
			.showThreadInfo(false)
			.tag(LOGGER_TAG)
			.build();
		Logger.addLogAdapter(new AndroidLogAdapter(formatStrategy)
		{
			@Override
			public boolean isLoggable(int priority, String tag)
			{
				return BuildConfig.DEBUG;
			}
		});
		Logger.i("Start app. Version: %s. Premium: "+premium, BuildConfig.VERSION_NAME);
	}
//	private AppComponent appComponent;

	public Boolean getPremium()
	{
		return premium;
	}
	
	public void setPremium(boolean premium)
	{
		this.premium = premium;
		prefs.edit().putBoolean(PREFS_PREMIUM, premium).apply();
	}
    
    /** null — первый запуск. false — бесплатный аккаунт, true — куплен premium */
    private Boolean premium;
    private SharedPreferences prefs;
    
    private static App app;

	@SuppressWarnings("WeakerAccess")
    public static final String LOGGER_TAG = "ASC";
    private static final String PREFS_PREMIUM = "premium";
}
